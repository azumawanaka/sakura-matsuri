<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SAKURA_MATSURI
 */

get_header();
?>

	<section class="wrp mb-90 mb-50-xs">
		<div class="banner banner-info"></div>

		<div class="banner-bottom bg-bluedot">
			<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/illus-6.png" alt="" class="illustrations is-tr is-tr2 wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">

			<div class="cntr">
				<h2 class="title-sakura_info wow fadeInUp" data-wow-duration="1.8s" style="visibility: visible; animation-duration: 1.8s; animation-name: fadeInUp;">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/illumination-txt.png" alt="" class="t_img">
				</h2>
				<div class="breacrumbs">
					<ul>
						<li>
							<span>
								さくらライトアップ情報
							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
	</section>
	
	<section class="wrp mb-180 mb-90-xs">
		<div class="pos-rel bg-white">
			
			<div class="cntr tc">

				<div class="cntr-865 plr-0-xs">
					<h2 class="title tc t-orange fs-60 fs-30-xs lh-80 lh-40-xs fw-500 mb-30 mb-20-xs wow fadeInUp" data-wow-duration="1s">
						<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sakura-info-title.png" alt="" class="is-wide">
					</h2>		
				</div>
				
			</div>

			<div class="cntr  pos-rel2 wow fadeInUp" data-wow-duration="1.5s">
				<p class="fs-18 fs-18-xs tc tl-xs lh-50 fw-500">
					春になると約1,000本の桜が咲き誇る舞鶴公園は、 <br class="v-pc">
					鴻臚館や福岡城、そして平和台野球場といったそれぞれの時代の象徴が<br class="v-pc">幾層にも重なった歴史的にも非常に貴重な意味を持つ公園です。<br class="v-pc">
					福岡藩祖の黒田官兵衛は先祖の故地・備前福岡（岡山県瀬戸内市）にちなんで、 <br class="v-pc">
					築城したこの地を“福岡”と名付けたと言われています。 <br class="v-pc">
					桜花爛漫うららかなこの季節に、城跡と桜のライトアップを通して、<br class="v-pc">
					福岡城400年の歴史と、福岡の繁栄を願った官兵衛の想いをお伝えいたします。
				</p>	

				<div class="gap gap-0-md gap-0-xs jc-c mb-40 mb-30-xs">
					<div class="md-3 xs-8">
						<div class="tc mt-40 mb-40 mt-30-xs mb-30-xs">
							<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/flower-yellow.png" alt="">
						</div>
						<p class="fs-18 fs-18-xs tc t-orange fw-800 mb-20 mb-10-xs">ライトアップ点灯時間</p>
						<p class="fs-28 fs-20-xs fw-800 tc">18:00〜22:00</p>
					</div>
				</div>

				<div class="btn-cntr mb-20 wow fadeInUp tc" data-wow-duration="1.2s">
	
					<ul class="btn-3cols">
						<li>
							<a href="<?php echo esc_url( home_url( '/information' ) ); ?>/?cat=4" class="btn bg-blue has-icon icon-arrow">さくら開花情報</a>
						</li>
						<li>
							<a href="<?php echo esc_url( home_url( '/access' ) ); ?>" class="btn bg-blue has-icon icon-arrow">アクセス</a>					
						</li>
						<li>
							<a href="https://goo.gl/maps/Ydpz5ATiegq" class="btn bg-pink100 has-icon icon-sqs">場内マップ</a>
						</li>
					</ul>

				</div>
			</div>

			<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/tower-l.png" alt="" class="illustrations is-left wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">

		</div>
	</section>
	<section class="wrp bg-bluedot">
		<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/cloud-form2.png" alt="" class="illustrations is-right v-pc">
		<div class="cntr pb-80 pb-40-xs pt-70 pt-35-xs">
			<div class="wow fadeInUp mb-50 mb-40-xs t-white" data-wow-duration="1s">
				<h3 class="fs-28 fs-18-xs t-brown mb-20 mb-15-xs fw-500">特別有料エリアライトアップ</h3>
				<p class="mb-30-xs">
					通常は夜間閉場しているエリアが、期間中は特別に開放されます。 <br>
					あでやかな夜桜と竹明かり、音などを組み合わせた空間が、訪れる人を夢幻の世界にいざないます。
				</p>

				<h4 class="fs-18 t-brown mt-30 mt-20-sp mb-10 mb-10-xs">開催時間</h4>
				<p class="fs-24 fs-18-xs fw-80 mb-20-xs">
					18:00〜22:00（入場は21:45まで）
				</p>

				<h4 class="fs-18 t-brown mt-30 mt-20-sp mb-10 mb-10-xs">入場料</h4>
				<p>
					1ヶ所…300円　　3ヶ所共通券…600円<br>
					※天守台周辺の桜園では、18時までは無料でお花見をお楽しみいただけますが、18時以降は有料となりますので予めご了承ください。
				</p>
			</div>
		</div>
	</section>
	<section class="wrp mb-80 mb-90-xs mt-100neg">
		<div class="cntr">
			<div class="gap gap-2-md gap-0-xs wow fadeInUp" data-wow-duration="2s">
				<div class="md-4 xs-12">
					<a href="#" class="card mb-30-xs no-hover">
						<div class="card-body">
							<div class="card-img">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/info-card-01.png" alt="" class="is-wide">
							</div>
							<div class="card-infos">
								<h4 class="title fs-18 fs-16-xs t-orange pt-10 pb-10">スーパードライ presents 桜園</h4>
								<p class="fs-14 t-dark">
									岡城の天守台跡から見下ろす絶景の夜桜が見れる桜園。満開の時の桜は圧巻の一言！この会場ではゆっくり花見をしながら、飲食を楽しむことができます。
								</p>
							</div>
						</div>
					</a>
				</div>
				<div class="md-4 xs-12 mb-30-xs">
					<a href="#" class="card  no-hover">
						<div class="card-body">
							<div class="card-img">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/info-card-02.png" alt="" class="is-wide">
							</div>
							<div class="card-infos">
								<h4 class="title fs-18 fs-16-xs t-orange pt-10 pb-10">多聞櫓</h4>
								<p class="fs-14 t-dark">
									江戸時代から城内に残っている国指定重要文化財の多聞櫓の白壁とソメイヨシノを幻想的にライトアップ。色鮮やかな桜と光のコントラストが情緒あふれる荘厳な空間へと誘います。
								</p>
							</div>
						</div>
					</a>
				</div>
				<div class="md-4 xs-12">
					<a href="#" class="card  no-hover">
						<div class="card-body">
							<div class="card-img">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/info-card-03.png" alt="" class="is-wide">
							</div>
							<div class="card-infos">
								<h4 class="title fs-18 fs-16-xs t-orange pt-10 pb-10">御鷹屋敷跡</h4>
								<p class="fs-14 t-dark">
									NHK大河ドラマ「軍師官兵衛」で全国的に有名となった、黒田官兵衛の隠居屋敷があったとされる御鷹屋敷跡では、荘厳な音楽に乗せて、様々な色に変化する幻想的な桜の空間を演出。
								</p>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	
	</section>

	<section class="wrp bg-pink2 pbt-50 pbt-25-xs">
		<div class="cntr">
			<div class="gap gap-5-md gap-0-xs ai-c">
				<div class="md-7 xs-12 mb-30-xs">
					<h3 class="title fs-28 fs-20-xs t-orange fw-500 mb-20 mb-15-xs">舞鶴公園のさくら</h3>
					<p class="mb-20 mb-20-xs">
						舞鶴公園は福岡市内屈指の桜の名所です。<br>
						一般的なソメイヨシノだけでなく、マツマエザクラ、ヨウコウ、カンザンなど、約1,000本の桜が植えられています。
					</p>
					<div class="tc-xs">
						<a href="<?php echo esc_url( home_url( '/information' ) ); ?>/?cat=4" class="btn bg-pink100 has-icon icon-sqs">詳しくはこちら</a>
					</div>
					
				</div>
				<div class="md-5 xs-12">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/info-card-04.png" alt="" class="is-wide mb-170neg">
				</div>
			</div>
		</div>
	</section>

	<section class="wrp bg-white mt-30 mt-80-xs mb-50 mb-20-xs tc">
		<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/petals.png" alt="" class="is-wide mt-60neg">
	</section>

	<section class="wrp">
		<div class="cntr">

			<h3 class="title lh-40 lh-30-xs is-border t-orange fs-24 fs-18-xs">周辺施設のご案内</h3>
			<div class="logos">
				<ul class="logo-list tc">
                    <li>
                        <div class="card mb-15-xs">
                            <a href="https://www.midorimachi.jp/maiduru/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-01.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">舞鶴公園</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="http://fukuokajyo.com/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-02.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">福岡城むかし探訪館</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="https://www.ohorikouen.jp/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-03.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">大濠公園/西公園</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="https://www.ohoriteien.jp/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-04.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">大濠公園日本庭園</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="http://www.ohori-nougaku.jp/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-05.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">大濠公園能楽堂</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="http://www.oohoriboathouse.jp/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-06.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">ボートハウス 大濠パーク</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="https://www.fukuoka-art-museum.jp/">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-07.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">福岡市美術館</span>
                        </div>
                    </li>
                </ul>
			</div>

		</div>
	</section>

<?php
get_footer();
