<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SAKURA_MATSURI
 */

get_header();
?>
	<section class="wrp mb-90 mb-50-xs">
		<!-- <div class="banner banner-info"></div> -->
		<section class="hero hero-inLang">
            <div class="hero-content tc pt-100">
                <div class="hero-img">
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/logo.png" alt="">
                </div>
            </div>

            <div class="banner-bottom bg-bluedot  is-flex">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/illus-3.png" alt="" class="illustrations is-tr wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">

				<div class="cntr">
					<div class="cntr-title">
						<h2 class="title fs-48 fs-20-xs t-orange is-wide lh-40 fw-500">
							福冈城樱花节
						</h2>
					</div>
				</div>
			</div>

    	</section>
		
		
	</section>
	
	<section class="wrp mt-150 mt-150-sp mb-180 mb-90-xs">
		<div class="pos-rel bg-white">
			
			<div class="cntr">
				<h2 class="title tc t-orange fs-36 fs-24-xs lh-48 lh-30-xs fw-500 mb-30 mb-20-xs wow fadeInUp" data-wow-duration="1s">
					在与黑田官兵卫有极深渊源的福冈城遗址上演以“城墙和樱花”<br class="v-pc">为主题的灯光秀。
				</h2>		
			</div>

			<div class="cntr  pos-rel2 wow fadeInUp" data-wow-duration="1.5s">
				<div class="gap gap-0-md gap-0-xs jc-c tc">
					<div class="md-9 xs-12">
						<p class="fs-18 fs-18-xs tc lh-48 lh-30-xs tl-xs">
							一到春天就约有1000株樱花灿烂盛开的舞鹤公园里还坐落着象征各自时代的鸿胪馆、福冈城、平和台棒球场等，是有着宝贵历史意义的公园。<br>
有这样一个说法，福冈藩祖黑田官兵卫当年建城之时，以他祖先的故乡—备前福冈（冈山县濑户内市）为缘由，将此地命名为“福冈”。<br>
在这个春和日丽、樱花烂漫的季节里，福冈城遗址携手樱花、灯光一起为游客们讲诉福冈城400年的悠久历史以及官兵卫一心想要繁荣福冈的美好夙愿。
						</p>	
					</div>
				</div>
			</div>
			<div class="wrp">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/tower-l.png" alt="" class="illustrations is-tr wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">
			</div>
		</div>
	</section>

	<section class="wrp bg-pink pad-l-sp pad-l-xs mb-180 mb-150-xs">
		<div class="cntr scroll_visible"  id="information">
			<h2 class="title is-abs pos-tl is-index wow fadeInUp is-sched" data-wow-duration="1.8s">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/schedule-txt.png" alt="" class="t_img">
			</h2>
		</div>
		<div class="cntr pb-80 pb-40-xs pt-70 pt-35-xs">
			<div class="gap gap-4-md gap-0-xs">
				<div class="md-8 xs-12">
					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">日程</h5>
					<h4 class="fs-20 fs-20-xs mb-15 mb-15-xs">
						2019年3月下旬～4月上旬<br>
                 ※具体日程将于3月中旬在官网上公布<br>
                 咨询服务时间：12：00～22：00
                 点灯时间：18：00～22：00　<br>※首日19:00～
						※日程根据樱花开花状况可能会有变更。


					</h4>
					<p class="fw-600 mb-20 mb-15-xs">※日程根据樱花开花状况可能会有变更。</p>

					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">观光问询处</h5>
					<h4 class="fs-20 fs-20-xs mb-20 mb-15-xs">
						3月31日～4月1日<br>
						4月6日～4月9日<br>
						时间：10：00～17：00　
					</h4>

					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">主办</h5>
					<p class="mb-20 mb-15-xs">
						福冈城樱花节实行委员会
					</p>
					<p>
						樱园（天守台）・多闻橹・御鹰宅第遗址特别灯展<br>
						夜幕下，古老的历史遗址在精心打造的灯光下呈现另一番风情。<br>
						时间：樱花节举办期间  18:00～22:00 ※首日19:00～<br>
                 费用：1处300日元，3处的通票600日元
					</p>

				</div>
				<div class="md-4 xs-12">
					<div class="r-img mt-30-xs mb-170neg  ov-flow-visible plr-4per-xs">
						<ul class="img-list v-pc">
							<li class="mb-30  mb--30-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-01.png" alt="" class="is-wide">
							</li>
							<li class="mb--60-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-02.png" alt="" class="is-wide">
							</li>
						</ul>
						<ul class="img-list-sp v-sp">
							<li class="mb-30 mb--30-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-01.png" alt="" class="is-wide">
							</li>
							<li class="mb--60-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-02.png" alt="" class="is-wide">
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="wrp bg-white pad-l-sp pad-l-xs">
		<div class="cntr scroll_visible"  id="information">
			<h2 class="title is-abs pos-tl is-index wow fadeInUp" data-wow-duration="1.8s">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/access-txt.png" alt="" class="t_img">
			</h2>
		</div>
		<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/flower-petals4.png" alt="" class="illustrations is-tr is-trPositive v-pc">
		<div class="cntr pb-80 pb-40-xs pt-70 pt-35-xs">

			<div class="gap gap-4-md gap-0-xs">
				<div class="md-9 xs-12">
					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">地点</h5>
					<h4 class="fs-20 fs-20-xs mb-15 mb-15-xs">国家指定历史遗址“福冈城遗址”（舞鹤公园）</h4>
					<p class="fw-600 mb-20 mb-15-xs">活动期间，周边道路可能出现交通拥挤情况，请尽量使用公共交通工具。</p>

					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">交通方式</h5>
					<p class="mb-40 mb-30-xs">
						地铁：<br>
						在“赤坂站”或“大濠公园站”下车后走路8分钟<br>
						西铁巴士：<br>
						在“福冈城·鸿胪馆前”、“福冈市美术馆东口”或“大手门·平和台田径赛场入口”下车后走路5～8分钟，在“赤坂3丁目”下车后走路10分钟
					</p>
					<div class="tc-xs">
						<a href="https://goo.gl/maps/SsjSjdxsFaH2" class="btn bg-pink100 has-icon icon-sqs">Google Map</a>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
