<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SAKURA_MATSURI
 */

get_header();
?>	
	<section class="wrp mb-90 mb-50-xs">
		<!-- <div class="banner banner-info"></div> -->
		<section class="hero hero-inLang">
            <div class="hero-content tc pt-100">
                <div class="hero-img">
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/logo.png" alt="">
                </div>
            </div>

            <div class="banner-bottom bg-bluedot  is-flex">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/illus-3.png" alt="" class="illustrations is-tr wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">

				<div class="cntr">
					<div class="cntr-title">
						<h2 class="title fs-48 fs-20-xs t-orange is-wide lh-40 fw-500">
							후쿠오카성 벚꽃축제
						</h2>
					</div>
				</div>
			</div>

    	</section>
		
		
	</section>
	
	<section class="wrp mt-150 mt-150-sp mb-180 mb-90-xs">
		<div class="pos-rel bg-white">
			
			<div class="cntr">
				<h2 class="title tc t-orange fs-36 fs-24-xs lh-48 lh-30-xs fw-500 mb-30 mb-20-xs wow fadeInUp" data-wow-duration="1s">
					구로다 칸베의 유서 깊은 후쿠오카성에서 <br class="v-pc">「성벽과 벚꽃」라이트 업
				</h2>		
			</div>

			<div class="cntr  pos-rel2 wow fadeInUp" data-wow-duration="1.5s">
				<div class="gap gap-0-md gap-0-xs jc-c tc">
					<div class="md-9 xs-12">
						<p class="fs-18 fs-18-xs tc lh-48 lh-30-xs tl-xs">
						봄이 되면 약 1,000그루의 벚나무가 화려하게 꽃을 피우는 마이즈루 공원은
                 고로칸과 후쿠오카성, 헤이와다이 야구장과 같은 각 시대의 상징들이 모여있는 역사적으로도 매우 중요한 의미를 지니는 공원입니다. <br>
             후쿠오카의 초대 번주였던 구로다 칸베가 선조의 땅　비젠후쿠오카(오카야마현 세토우치시)를 기념하기 위해서, 성이 세워진 이 곳에 “후쿠오카”라는 이름을 붙였다고 합니다.  <br>
                  벚꽃이 만발하는 화창한 이 계절에 성터와 벚꽃의 라이트업을 통해서
                     후쿠오카성의 400년 역사와 후쿠오카 번영을 바랬던 칸베의 마음을 전하고자 합니다. 

						</p>	
					</div>
				</div>
			</div>
			<div class="wrp">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/tower-l.png" alt="" class="illustrations is-tr wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">
			</div>
		</div>
	</section>

	<section class="wrp bg-pink pad-l-sp pad-l-xs  mb-180 mb-180-xs">
		<div class="cntr scroll_visible"  id="information">
			<h2 class="title is-abs pos-tl is-index wow fadeInUp is-sched" data-wow-duration="1.8s">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/schedule-txt.png" alt="" class="t_img">
			</h2>
		</div>
		<div class="cntr pb-80 pb-40-xs pt-70 pt-35-xs">
			<div class="gap gap-4-md gap-0-xs">
				<div class="md-8 xs-12">
					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">일정</h5>
					<h4 class="fs-20 fs-20-xs mb-20 mb-15-xs">
						2019년3월 하순~4월 초순<br>
                  일정은 3월 중순 이후, 홈페이지(메인 페이지)에서 확인하실 수 있습니다.
                  안내소 개설시간/12:00~22:00<br>
                  라이트업 점등시간/18:00~22:00 ※첫날은 19:00~
                  ※벚꽃 개화상태에 따라 일정이 변경되는 경우가 있습니다. 
					</h4>

					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">장소</h5>
					<p>
						The overseas tourist information booths for customers by a volunteer staff in Fukuoka　City are established
					</p>
					<h4 class="fs-20 fs-20-xs mb-20 mb-15-xs">
						국가지정사적「후쿠오카 성터」(마이즈루 공원)
                  행사장 주변 도로가 대단히 혼잡하오니, 오실 때는 대중교통을 이용해 주십시오.
					</h4>

					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">주최</h5>
					<p class="mb-20 mb-15-xs">
						후쿠오카성 벚꽃축제 실행위원회

					</p>
					<p>
						사쿠라엔(천수대)・다몬 망루・오타카 저택 흔적지 특별 라이트업<br>
						밤의 사적을 특별한 연출로 라이트업.<br>
						일시: 벚꽃축제 기간중18:00~22:00  ※첫날은 19:00~<br>
						요금:1곳당 300엔, 3곳 공동권은 600엔
					</p>

				</div>
				<div class="md-4 xs-12">
					<div class="r-img mt-30-xs mb-170neg ov-flow-visible plr-4per-xs">
						<ul class="img-list v-pc">
							<li class="mb-30 mb--30-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-01.png" alt="" class="is-wide">
							</li>
							<li class="mb--60-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-02.png" alt="" class="is-wide">
							</li>
						</ul>
						<ul class="img-list-sp v-sp">
							<li class="mb-30 mb--30-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-01.png" alt="" class="is-wide">
							</li>
							<li class="mb--60-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-02.png" alt="" class="is-wide">
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="wrp bg-white pad-l-sp pad-l-xs">
		<div class="cntr scroll_visible"  id="information">
			<h2 class="title is-abs pos-tl is-index wow fadeInUp" data-wow-duration="1.8s">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/access-txt.png" alt="" class="t_img">
			</h2>
		</div>
		<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/flower-petals4.png" alt="" class="illustrations is-tr is-trPositive v-pc">
		<div class="cntr pb-80 pb-40-xs pt-70 pt-35-xs">

			<div class="gap gap-4-md gap-0-xs">
				<div class="md-9 xs-12">
					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">장소</h5>
					<h4 class="fs-20 fs-20-xs mb-15 mb-15-xs">국가지정사적「후쿠오카 성터」(마이즈루 공원)</h4>
					<p class="fw-600 mb-20 mb-15-xs">행사장 주변 도로가 대단히 혼잡하므로 오실 때는 대중교통을 이용해주세요.</p>

					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">교통안내</h5>
					<p class="mb-40 mb-30-xs">
						지하철<br>
　　　　　　　　　　　　　　　　　　　　　　　「아카사카 역」「오호리코엔 역」하차, 도보 8분<br>
						니시테츠 버스<br>
						「후쿠오카죠・고로칸 마에」「후쿠오카시 비쥬쯔칸 히가시구치」「오테몬・헤이와다이 리쿠죠 쿄기죠 이리구치」하차, 도보 5~8분
　　　　　　　　　　　　　　　　　　　　　　　「아카사카3초메」하차, 도보 10분
					</p>
					<div class="tc-xs">
						<a href="https://goo.gl/maps/SsjSjdxsFaH2" class="btn bg-pink100 has-icon icon-sqs">Google Map</a>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
