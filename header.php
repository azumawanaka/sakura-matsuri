<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SAKURA_MATSURI
 */

?>

<!doctype html>

<html lang="ja">

    <head>
    	<meta charset="utf-8" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="icon" type="image/vnd.microsoft.icon" href="">
		<meta name="keywords" content="福岡,福岡城跡,舞鶴公園,さくら,桜,ライトアップ,おおほりまつり,お花見" />
		<meta property="og:title" content="福岡城「さくらまつり」" />
		<meta property="og:type" content="website" />
		<meta property="og:description" content="～城壁とさくらのライトアップ～福岡城「さくらまつり」、今年は3月24日（土）から4月8日（日）までの開催予定です。" />
		<meta property="og:url" content="http://saku-hana.jp/" />
		<meta property="og:image" content="http://saku-hana.jp/common/img/facebook.jpg" />
		<meta property="og:site_name" content="福岡城「さくらまつり」" />
    	
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.3.0/animate.css">
         <link rel="shortcut icon" href="<?php echo get_template_directory_uri()?>/assets/img/icons/favicon-xs.png" type="image/x-icon">
        <!-- scripts -->
        
        <?php wp_head(); ?>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/jquery.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/main.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/custom.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    </head>

    <!--[if gt IE 10]>-->
    <script>

          wow = new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       0,          // default
          mobile:       true,       // default
          live:         true        // default
        }
    )
    wow.init();

    var is_home = true;

    </script>
    <!--<![endif]-->
    <script>
        var tem_dir = '<?php echo get_template_directory_uri(); ?>';
    </script>
    <?php $is_home = ""; if(is_home() || is_front_page()) { 
        $is_home = "is-home";
     } ?>
<body>

    <section class="top-head">
        <div class="head">
            <header class="header-main">
                <h3 class="logo <?php echo $is_home; ?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri()?>/assets/img/icons/logo.png" alt="logo"></a></h3>
            </header>
            <a href="javascript:;" id="toggle-nav">
                <span></span>
                <span></span>
                <span></span>
            </a>
            <nav class="nav-main is-sp">
                <ul class="nav-item-l">
                    <li>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" <?php if(is_home()) { echo "class='is-active'"; } ?>>トップ</a>
                    </li>
                    <li class="dropDown">
                        <span class="drop-icon toggle-sub"></span>
                        <a href="<?php echo esc_url( home_url( '/information' ) ); ?>" class="parent-link">イベント・開花情報</a>
                        <ul class="sub-items">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/information/?cat=2' ) ); ?>">新着ニュース</a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url( home_url( '/information/?cat=3' ) ); ?>">イベント</a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url( home_url( '/information/?cat=4' ) ); ?>">さくら開花</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo esc_url( home_url( '/sakura-info' ) ); ?>"  class="<?php if(is_page('sakura-info')) { echo "is-active"; } ?>">さくらライトアップ情報</a>
                    </li>
                    <li>
                        <a href="<?php echo esc_url( home_url( '/gourmet' ) ); ?>"  class="<?php if(is_page('gourmet')) { echo "is-active"; } ?>">グルメ・BBQ</a>
                    </li>
                    <li>
                        <a href="<?php echo esc_url( home_url( '/access' ) ); ?>"   class="<?php if(is_page('access')) { echo "is-active"; } ?>">アクセス</a>
                    </li>
                </ul>
                <ul class="nav-item-r">
                    <li class="dropDown">
                        <span class="drop-icon toggle-sub"></span>
                        <?php if(is_page('english')) { ?>
                            <a href="javascript:;" class="parent-link">EN</a>
                            <ul class="sub-items">
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">JP</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/chinese' )); ?>">中文</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url('/') ); ?>">한국</a>
                                </li>
                            </ul>
                        <?php }else if(is_page('chinese')) { ?>
                            <a href="javascript:;" class="parent-link">中文</a>
                            <ul class="sub-items">
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">JP</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/english' ) ); ?>">EN</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url('/korean') ); ?>">한국</a>
                                </li>
                            </ul>
                        <?php }else if(is_page('korean')) { ?>
                            <a href="javascript:;" class="parent-link">한국</a>
                            <ul class="sub-items">
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">JP</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/english' ) ); ?>">EN</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url('/chinese') ); ?>">中文</a>
                                </li>
                            </ul>
                        <?php }else{ ?>
                            <a href="javascript:;" class="parent-link">JP</a>
                            <ul class="sub-items">
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/english' ) ); ?>">EN</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/chinese' ) ); ?>">中文</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url('/') ); ?>">한국</a>
                                </li>
                            </ul>
                        <?php } ?>

                    </li>
                    <li class="nav-icons">
                        <a href="https://twitter.com/infosakura2012" target="_blank" class="is-tw">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/twitter.png" alt="">
                        </a>
                        <a href="https://www.facebook.com/infosakura2012/?ref=br_rs" target="_blank" class="is-fb">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/fb.png" alt="">
                        </a>
                        <a href="https://www.instagram.com/fukuoka_sakuramatsuri/" target="_blank" class="is-fb">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/instagram.png" alt="">
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>