<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SAKURA_MATSURI
 */

get_header();
?>


	<section class="wrp mb-90 mb-50-xs">
		<div class="banner banner-access"></div>

		<div class="banner-bottom bg-bluedot">
			<img src="<?php echo get_template_directory_uri();  ?>/assets/img/icons/illus-6.png" alt="" class="illustrations is-tr is-tr2 wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">
			<div class="cntr">
				<h2 class="title for-lower access-title wow fadeInUp" data-wow-duration="1.8s" style="visibility: visible; animation-duration: 1.8s; animation-name: fadeInUp;">
					<img src="<?php echo get_template_directory_uri();  ?>/assets/img/cards/access-txt.png" alt="" class="t_img">
				</h2>
				<div class="breacrumbs">
					<ul>
						<li>
							<span>
								交通アクセス情報
							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="wrp bg-white">
		<div class="cntr">
			<div class="gap gap-2-md gap-0-xs mb-100 mb-50-xs">
				<div class="md-5 xs-12">
					<div class="article-infos wow fadeInUp" data-wow-duration="1.5s">
						<h4 class="fs-22 fs-18-xs t-orange mb-50 mb-30-xs fw-800">
							会場周辺の道路は大変混雑します。<br>
							駐車場は台数に限りがありますので、<br class="v-pc">
							公共交通機関をご利用ください。
						</h4>
						<h4 class="fs-28 fs-24-xs t-orange mb-18 mb-15-xs">所在地</h4>
						<p class="fs-16 mb-50 mb-30-xs fw-500">
							〒810-0043 福岡市中央区城内1
						</p>

						<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-18 mb-15-xs"><span>地下鉄でお越しの場合</span></h5>
						<p class="fs-16 fs-16-xs mb-30 mb-20-xs fw-500">
							「赤坂」「大壕公園」下車徒歩8分<br>
							福岡市交通局サイトは<a href="https://subway.city.fukuoka.lg.jp/" target="_blank" class="t-pink2">こちら</a>
						</p>	

						<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-18 mb-15-xs"><span>西鉄バスでお越しの場合</span></h5>
						<p class="fs-16 fs-16-xs mb-30 mb-20-xs fw-500">
							・「福岡城・鴻臚館前」「福岡市美術館東口」<br class="v-pc">
							「大手門・平和台陸上競技場入口」下車 徒歩5~8分<br class="v-pc">
							・「赤坂3丁目」下車 徒歩10分
						</p>	
						
					</div>
				</div>
				<div class="md-7 xs-12 tr wow fadeInUp tc-xs" data-wow-duration="1s">
					<img src="<?php echo get_template_directory_uri();  ?>/assets/img/cards/gmap.png" alt="" class="is-wide mb-15 mb-15-xs">
					<a href="https://goo.gl/maps/Ydpz5ATiegq" class="btn bg-pink100 has-icon icon-sqs mw-250">Googleマップで見る</a>
				</div>
			</div>		
		</div>
	</section>

	<section class="wrp bg-pink2 pt-50 pt-30-xs pb-100 pb-80-xs">
		<div class="cntr">
			<img src="<?php echo get_template_directory_uri();  ?>/assets/img/icons/tower.png" alt="" class="illustrations is-tr v-pc">
			<div class="gap gap-5-md gap-0-xs">
				<div class="md-8 xs-12">
					<div class="article-infos wow fadeInUp" data-wow-duration="1s">

						<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-18 mb-15-xs"><span>駐車場案内</span></h5>
						<h6 class="fs-16 fs-16-xs t-blue">第１駐車場・第2駐車場</h6>
						<p class="mb-30 mb-20-xs fw-500">
							収容可能台数…139台（第１駐車場67台第2駐車場72台）<br>
							料金：１時間150円<br>
							入庫時間（福岡城さくらまつり期間中）<br>
							第１駐車場：入庫6:30〜22:00 まで※出庫は24時間可能(4月1日以降は5:30〜)<br>
							第2駐車場：入庫8:00〜22:00 まで※出庫は24時間可能
						</p>	

						<h6 class="fs-16 fs-16-xs t-blue">タイムズ福岡城三の丸駐車場</h6>
						<p class="mb-30 mb-20-xs fw-500">
							収容可能台数…256台（一般241台・バス15台）<br>
							料金：<br>
							一般…1時間200円（当日最大料金400円）<br>
							バス…60分1000円駐車後24時間最大料金3000円<br>
							入庫時間…7:00〜21:00まで
						</p>	
					</div>
				</div>
				
			</div>	
			<h5 class="title t-orange fs-16 fs-16-xs"><span>会場周辺の道路は大変混雑します。駐車場は台数に限りがありますので、公共交通機関をご利用ください。</span></h5>
			<p class="fw-500">
				※第1駐車場・第2駐車場の福岡城さくらまつり期間以外の駐車場営業時間については<a href="#" target="_blank" class="t-pink2">こちら</a>をこ覧ください。
			</p>	
		</div>
	</section>

<?php
get_footer();
