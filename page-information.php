<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SAKURA_MATSURI
 */

get_header();
$post_category = @$_GET['cat'];
if(empty($post_category)) {
	$post_category = "test";
}else{
	$post_category = @$_GET['cat'];
	if($post_category!=3) {
		$display= "display: none";
	}
}

?>
	
	<section class="wrp mb-90 mb-50-xs">
		<div class="banner banner-information"></div>

		<div class="banner-bottom bg-bluedot">
			<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/illus-5.png" alt="" class="illustrations is-tr is-tr2 wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">
			<div class="cntr">
				<h2 class="title-sakura_info wow fadeInUp" data-wow-duration="1.8s" style="visibility: visible; animation-duration: 1.8s; animation-name: fadeInUp;">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/info-txt.png" alt="" class="t_img">
				</h2>
				<div class="breacrumbs">
					<ul>
						<li>
							<span>
								イベント・開花情報
							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="wrp bg-white mb-100 mb-80-xs">
		
		<div class="cntr wow fadeInUp" data-wow-duration="1s">

			<div class="information-pickup" style="<?php echo $display; ?>">
                <div class="gap gap-5-md gap-0-xs mb-40 mb-30-xs sakura_masonry">

                        <?php 
                            $paged2 = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                            $wpb_all_query2 = new WP_Query(
                                        array(
                                            'post_type'     =>'article', 
                                            'post_status'   =>'publish', 
                                            'posts_per_page' => -1, 
                                            'orderby'        => 'publish_date',
                                            'order'         => 'DESC',
                                            'paged'         => $paged2
                                        ));  

                            ?>

                          <?php if ( $wpb_all_query2->have_posts() ) : ?>
                          <!-- the loop -->
                            

                          <?php while ( $wpb_all_query2->have_posts() ) : $wpb_all_query2->the_post();  ?>
                            <?php 
                                    $categories2 = get_the_terms( $post->ID, 'categories' );
                                    // get term id
                                    $cat_tID2 = $categories2[0]->term_id;
                                    $img_name2 = "flower-green.png";

                                    $article_date2 = get_the_date('Y.m.d');
              
                                    if($cat_tID2==3 && get_post_meta($post->ID,'input1', true)==1) {
                                        $img_name2 = "flower-green.png";
                                    
                                ?>
                                <div class="md-4 xs-12 mb-90 mb-50-xs sakura_item"  data-attr="<?php echo $cat_tID; ?>">    
                                
                                    <a href="<?php the_permalink(); ?>" class="card">
                                        <div class="card-body">
                                            <div class="card-img">
                                                <div class="flwr-icon">
                                                    
                                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/<?php echo $img_name2; ?>" alt="">
                                                    
                                                </div>

                                                <?php if(has_post_thumbnail()){ ?>
                                                    <img class="is-wide" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                                                <?php }else{ ?>

                                                    <img  class="is-wide" src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-green.png" alt="<?php the_title(); ?>"">
                                                <?php } ?>

                                            </div>

                                               <?php if(get_post_meta($post->ID,'schedule1', true)) {
                                                    echo '<div class="mt-5">'.get_post_meta($post->ID,'schedule1', true).'</div>';
                                                }
                                                if(get_post_meta($post->ID,'input1', true)==1) {
                                                    echo '<span class="btn is-border pickup">開催中</span>';
                                                }  ?>

                                            <h4 class="info-title"><?php the_title(); ?></h4>
                                            <div class="info-txt">
                                                <?php $content2 = get_the_content(); 
                                                    // strip tags to avoid breaking any html
                                                    $string2 = strip_tags($content2);
                                                    if (strlen($string2) > 110) {

                                                        // truncate string
                                                        $stringCut2 = mb_strcut($string2, 0, 110);
                                                        $endPoint2 = strrpos($stringCut2, ' ');

                                                        //if the string doesn't contain any space then it will cut without word basis.
                                                        $string2 = $endPoint2? mb_strcut($stringCut2, 0, $endPoint2) : mb_strcut($stringCut2, 0);
                                                        $string2 .= '...';
                                                    }
                                                    echo $string2;

                                                 ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>     

                                <?php } ?>     
                          <?php endwhile; ?>

                      <!-- end of the loop -->

                    <?php endif; ?>
                </div>

            </div>

			<ul class="cat__btn-list mb-70 mb-50-xs jc-c" id="p_info">
			<?php
				$include_taxonomy = array();
			    $args = array(
			               'taxonomy' => 'categories',
			               'orderby' => 'term_id',
			               'order'   => 'ASC'
			           );
			    $categories_taxonomy = get_categories($args); 
			    $cats = get_categories($args);
			    $img_name = "is-green";
			   foreach($cats as $cat) {

			   	if($cat->term_id==3) {
                    $img_name = "is-green";
                } else if ($cat->term_id==4) {

                    $img_name = "is-pink";
                } else {
                    $img_name = "is-blue";
                } 
			?>
				<li>
					<a href="" class="btn is-border <?php echo $img_name; ?>" own-attr="<?php echo $cat->term_id; ?>" cat-attr="<?php echo  $post_category; ?>"><?php echo $cat->name; ?></a>
				</li>
			<?php
			   }
			?>
			</ul>
			<div class="information-lists">
				
	          	<div class="gap gap-5-md gap-0-xs mb-40 mb-30-xs sakura_masonry">		

	          			<?php 
		                		$count = 0;
		                		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		                		$max_post_page = 9;
		                		
					          		$wpb_all_query = new WP_Query(
			          				array(
			          					'post_type'		=>'article', 
			          					'post_status'	=>'publish', 
			          					'posts_per_page'=> $max_post_page,
										'orderby'  => 'publish_date',
										'order'	=> 'DESC',
			          					'paged'         => $paged,
			          					'tax_query' => [
									        [
									            'taxonomy' => 'categories',
									            'field' => 'term_id',
									            'terms' => $post_category,
									            'order'	=> 'DESC',
									            'include_children' => true 
									        ]
									    ]
			          				)); 

			          				$total_pages = $wpb_all_query->max_num_pages;
			          	?>

			        	<?php if ( $wpb_all_query->have_posts() ) : ?>
					          
					          <!-- the loop -->
					          	
						          	<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); $count++; ?>
						         		<div class="md-4 xs-12 mb-40  mb-50-xs sakura_item">
						         			<?php 
												$categories = get_the_terms( $post->ID, 'categories' );
							                    // get term id
							                    $cat_tID = $categories[0]->term_id;
							                    $img_name = "flower-green.png";

							                    $article_date = get_the_date('Y.m.d');

							                    if($cat_tID==3) {
							                    	$img_name = "flower-green.png";

							                    } else if ($cat_tID==4) {

							                    	$img_name = "flower-pink.png";
							                    } else {
							                    	$img_name = "flower-blue.png";
							                    } 
							                ?>
											<a href="<?php the_permalink(); ?>" class="card">
												<div class="card-body">
													<div class="card-img">
														<div class="flwr-icon">
															
															<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/<?php echo $img_name; ?>" alt="">

														</div>

														<?php if(has_post_thumbnail()){ ?>
															<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="is-wide" />
														<?php }else{
															if($cat_tID==2) {
															?>
															<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-04.png" alt="<?php the_title(); ?>"  class="is-wide" />
														<?php } else if($cat_tID==3) { ?>
															<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-green.png" alt="<?php the_title(); ?>" class="is-wide" />
															<?php } else if($cat_tID==4) { ?>
															<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-pink.png" alt="<?php the_title(); ?>" class="is-wide" />
															<?php }
														} ?>

													</div>
													<?php if($cat_tID!==3) { ?>
														<!-- <p class="info-date"><?php echo $article_date; ?></p> -->
													<?php }else{
														if(get_post_meta($post->ID,'schedule1', true)) {
			                                                echo '<div class="mt-5">'.get_post_meta($post->ID,'schedule1', true).'</div>';
			                                            }
														if(get_post_meta($post->ID,'input1', true)==1) {
															echo '<span class="btn is-border pickup">開催中</span>';
														}
														
													} ?>
													<h4 class="info-title"><?php the_title(); ?></h4>
													<div class="info-txt">
														<?php $content = get_the_content(); 
															// strip tags to avoid breaking any html
															$string = strip_tags($content);
															if (strlen($string) > 110) {

															    // truncate string
															    $stringCut = mb_strcut($string, 0, 110);
															    $endPoint = strrpos($stringCut, ' ');

															    //if the string doesn't contain any space then it will cut without word basis.
															    $string = $endPoint? mb_strcut($stringCut, 0, $endPoint) : mb_strcut($stringCut, 0);
															    $string .= '...';
															}
															echo $string;
														 ?>
													</div>
												</div>
											</a>
											
										</div>
						          	<?php endwhile; ?>

						          		<!-- end of the loop -->
					        
								</div>
						<?php else: ?>
			            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			        	<?php endif; ?>


			        	<?php if( $total_pages > 1){ ?>
							<div class="info-pagination tc">
								<ul class="page-numbers">
									<li class="first"><a href="<?php echo get_pagenum_link(1); ?>"></a></li>
								</ul>
									  <?php 

									    if ($total_pages > 1){

									        $current_page = max(1, get_query_var('paged'));
									        $big = 999999999; // need an unlikely integer
									        echo paginate_links(array(
									            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
									            'format' => '?page=%#%',
									            'current' => $current_page,
									            'total' => $total_pages,
									            'prev_text'    => __(''),
									            'next_text'    => __(''),
									            'type'	=> 'list'
									        ));
									    }    

										wp_reset_postdata();

						           ?>
						        <ul class="page-numbers">
									<li class="last"><a href="<?php echo get_pagenum_link($total_pages); ?>"></a></li>
								</ul>   
							</div>
						<?php } ?>

		        </div>


			</div>
			
		</div>
	</section>

<?php
get_footer();
