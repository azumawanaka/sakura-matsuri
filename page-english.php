<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SAKURA_MATSURI
 */

get_header();
?>
	
	<section class="wrp mb-90 mb-50-xs">
		<!-- <div class="banner banner-info"></div> -->
		<section class="hero hero-inLang">
            <div class="hero-content tc pt-100">
                <div class="hero-img">
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/logo.png" alt="">
                </div>
            </div>

            <div class="banner-bottom bg-bluedot">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/illus-3.png" alt="" class="illustrations is-tr wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">

				<div class="cntr">
					<div class="cntr-title">
						<h2 class="title fs-48 fs-20-xs t-orange is-wide mb-10 lh-40 fw-500">
							Fukuoka Castle Sakura Festival
						</h2>
						<p class="fs-18 fs-14-xs t-orange is-wide">A dance between cherry blossoms and illumination at Fukuoka Castle</p>
					</div>
				</div>
			</div>

    	</section>
		
		
	</section>
	
	<section class="wrp mt-150 mt-150-sp mb-180 mb-90-xs">
		<div class="pos-rel bg-white">
			
			<div class="cntr">
				<h2 class="title tc t-orange fs-36 fs-24-xs lh-48 lh-30-xs fw-500 mb-30 mb-20-xs wow fadeInUp" data-wow-duration="1s">
					Blossoms & Battlements: light-up event <br class="v-pc">
					at Kuroda Kanbei’s Fukuoka Castle
				</h2>		
			</div>

			<div class="cntr  pos-rel2 wow fadeInUp" data-wow-duration="1.5s">
				<div class="gap gap-0-md gap-0-xs jc-c tc">
					<div class="md-9 xs-12">
						<p class="fs-18 fs-18-xs tc lh-48 lh-30-xs tl-xs">
							Every spring, around 1,000 beautiful cherry blossom trees come into bloom in Maizuru Park.The park has valuable historical significance, with the Korokan Museum, Fukuoka Castle and Heiwadai Baseball Stadium, each symbolizing a different era, being located on site.
Kuroda Kanbei, founder of the Kuroda Clan, is said to have named the area where he constructed his castle "Fukuoka", after his ancestors' homeland: Bizen-Fukuoka (present-day Setouchi City, Okayama Prefecture).
Come and enjoy the battlements of Fukuoka Castle lit up amongst the veils of falling cherry blossoms this season, and experience the 400-year history of Fukuoka Castle and Kuroda Kanbei’s vision for the prosperity of Fukuoka.  

						</p>	
					</div>
				</div>
			</div>
			<div class="wrp">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/tower-l.png" alt="" class="illustrations is-tr wow fadeInUp" data-wow-duration="2s" data-wow-offset="100">
			</div>
		</div>
	</section>

	<section class="wrp bg-pink pad-l-sp pad-l-xs  mb-180 mb-150-xs">
		<div class="cntr scroll_visible"  id="information">
			<h2 class="title is-abs pos-tl is-index wow fadeInUp is-sched" data-wow-duration="1.8s">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/schedule-txt.png" alt="" class="t_img">
			</h2>
		</div>
		<div class="cntr pb-80 pb-40-xs pt-70 pt-35-xs">
			<div class="gap gap-4-md gap-0-xs">
				<div class="md-8 xs-12">
					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">Festival Dates</h5>
					<h4 class="fs-20 fs-20-xs mb-15 mb-15-xs">From late March to early April 2019</h4>
					<p class="fw-600 mb-20 mb-15-xs">※The festival schedule will be announced on our website in the middle of March.</p>
					<p class="mb-30 mb-20-xs">
						Information Desk Opening Hours: <br class="v-sp">12:00–22:00<br>
						Illumination Hours: <br class="v-sp">18:00 - 22:00 <br class="v-sp">
						※The illuminations will start from 19:00 on the first day.
						※Dates may be subject to change depending on the conditions of the blossoms.
					</p>
					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">Special Illumination</h5>
					<p class="mb-20 mb-15-xs">
						Special Light-Up Events at Sakura-en Garden (located at the base of the main keep), Tamon turret, and the site of the Otaka estate 
                  Special night time illumination events at historical sites.
					</p>
					<p>
						Time: 18:00–22:00 during the Cherry Blossom Festival<br>
						※The illuminations will start from 19:00 on the first day.
                 Fee: 300 yen per venue, 600 yen for a multi-ticket for 3 venues
					</p>

				</div>
				<div class="md-4 xs-12">
					<div class="r-img mt-30-xs mb-170neg ov-flow-visible plr-4per-xs">
						<ul class="img-list v-pc">
							<li class="mb-30 mb--30-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-01.png" alt="" class="is-wide">
							</li>
							<li class="mb--60-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-02.png" alt="" class="is-wide">
							</li>
						</ul>
						<ul class="img-list-sp v-sp">
							<li class="mb-30 mb--30-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-01.png" alt="" class="is-wide">
							</li>
							<li class="mb--60-xs">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/sched-02.png" alt="" class="is-wide">
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="wrp bg-white pad-l-sp pad-l-xs">
		<div class="cntr scroll_visible"  id="information">
			<h2 class="title is-abs pos-tl is-index wow fadeInUp" data-wow-duration="1.8s">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/access-txt.png" alt="" class="t_img">
			</h2>
		</div>
		<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/flower-petals4.png" alt="" class="illustrations is-tr is-trPositive v-pc">
		<div class="cntr pb-80 pb-40-xs pt-70 pt-35-xs">

			<div class="gap gap-4-md gap-0-xs">
				<div class="md-9 xs-12">
					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">Location</h5>
					<h4 class="fs-20 fs-20-xs mb-15 mb-15-xs">Fukuoka Castle Remains, a nationally designated historical site (Maizuru Park)</h4>
					<p class="fw-600 mb-20 mb-15-xs">*Please use public transportation when coming to the park to avoid heavy traffic around the area during the festival.</p>

					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">Subway</h5>
					<p class="mb-20 mb-15-xs">
					   8-minute walk from Akasaka or Ohorikoen subway stations.
					</p>
					<h5 class="fs-18 fs-18-xs mb-15 mb-15-xs fw-500 t-orange">Nishitetsu Bus</h5>
					<p class="mb-40 mb-30-xs">
						5 to 8-minute walk from Fukuokajo Korokan-mae, Fukuoka-shi Bijutsukan Higashi-guchi, and Otemon/Heiwadai Rikujo-kyogijo-iriguchi bus stops.
<br>
						10-minute walk from Akasaka 3-chome bus stop
					</p>
					<div class="tc-xs">
						<a href="https://goo.gl/maps/SsjSjdxsFaH2" class="btn bg-pink100 has-icon icon-sqs">Google Map</a>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
