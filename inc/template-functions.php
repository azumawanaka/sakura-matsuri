<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package TOHO_KAGU
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function toho_kagu_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'sakura_matsuri_body_classes' );

/**
* Add active class to current page link
*/

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('nav-item-l', $classes) || in_array('current-menu-item', $classes) ){
        $classes[] = 'selected ';
    }
    return $classes;
}


// change checkbox taxonomy
function checktoradio(){
    echo '<script type="text/javascript">jQuery("#categorieschecklist-pop input, #categorieschecklist input, .cat-checklist input").each(function(){this.type="radio"});</script>';
}

add_action('admin_footer', 'checktoradio');

// register information custom post
function register_cpt_article() {
	$labels = [
		'name' => 'インフォメーション',
		'singular_name' => 'Article',
		'add_new_item' => 'Add New インフォメーション',
		'edit_item' => 'Edit インフォメーション'
	];

	$args = [
		'labels' 				=> $labels,
		'rewrite' => array( 'slug' => 'information/list'),
		'show_ui' 				=> true,
		'show_in_rest' 			=> true,
		'show_in_nav_menus'		=> true,
		'public'				=> true,
		'has_archive' 			=> false,
		'publicly_queryable' 	=> true,
		'query_var' 			=> true,
		'menu_icon'				=> 'dashicons-format-status',
		'supports' 				=> ['title', 'editor', 'thumbnail', 'post-formats']
	];

	register_post_type('article', $args);

	register_taxonomy( 'categories', array('article'), array(
	       'hierarchical' => true, 
	       'label' => 'Categories', 
	       'singular_label' => 'Category', 
	       'rewrite' => array( 'slug' => 'categories', 'with_front'=> false )
	       )
	   );

	   register_taxonomy_for_object_type( 'categories', 'article' ); 

}

add_action('init', 'register_cpt_article');


// checkbox
function article_custom_metabox() {

	add_meta_box(
		'article_meta',
		__( 'Information List Checkbox' ),
		'article_metabox_callback',
		'article',
		'side',
		'high'
	);
}


add_action( 'add_meta_boxes', 'article_custom_metabox' );

function article_metabox_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'article_once' );
	$wl_stored_meta = get_post_meta( $post->ID );

    $input1 = (!empty($wl_stored_meta['input1']) ? esc_attr(  $wl_stored_meta['input1'][0] ) : null);

    ?>

	<div class="form_group article-form-box">
		<input type="checkbox" name="input1" id="input1" value="<?php echo $input1; ?>" <?php if($input1==1){ echo 'checked'; } ?>> 開催中
	</div>

    <?php
}

function article_meta_save( $post_id) {
	$is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );

    $is_valid_nonce_input1 = ( isset( $_POST[ 'input1' ] ) && wp_verify_nonce( $_POST[ 'input1' ], basename( __FILE__ ) ) ) ? 'true' : 'false';


    if ( $is_autosave || $is_revision || !$is_valid_nonce_input1 ) {
        return;
    }

    if ( isset( $_POST[ 'input1' ] ) ) {
    	update_post_meta( $post_id, 'input1', 1 );
    }else{
    	update_post_meta( $post_id, 'input1', 0 );
    }
    
}
add_action( 'save_post', 'article_meta_save' );


// event category inputs
function event_custom_metabox() {

	add_meta_box(
		'event_meta',
		__( 'Event Category Inputs' ),
		'event_metabox_callback',
		'article',
		'normal',
		'high'
	);
}


add_action( 'add_meta_boxes', 'event_custom_metabox' );

function event_metabox_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'event_once' );
	$wl_stored_meta = get_post_meta( $post->ID );
	$schedule1 = (!empty($wl_stored_meta['schedule1']) ? esc_attr(  $wl_stored_meta['schedule1'][0] ) : null);
    $schedule2 = (!empty($wl_stored_meta['schedule2']) ? esc_attr(  $wl_stored_meta['schedule2'][0] ) : null);
    $venue = (!empty($wl_stored_meta['venue']) ? esc_attr(  $wl_stored_meta['venue'][0] ) : null);
    $price = (!empty($wl_stored_meta['price']) ? esc_attr(  $wl_stored_meta['price'][0] ) : null);

    ?>

	<style>
    	.staff-form-box {
    		max-width: 500px;
    		width: 100%;
    		position: relative;
    	}
    	.gap {
		  display: -webkit-box;
		  /* OLD - iOS 6-, Safari 3.1-6 */
		  /* OLD - Firefox 19- (buggy but mostly works) */
		  display: -ms-flexbox;
		  /* TWEENER - IE 10 */
		  display: -webkit-flex;
		  /* NEW - Chrome */
		  display: flex;
		  -webkit-flex-wrap: wrap;
		      -ms-flex-wrap: wrap;
		          flex-wrap: wrap;
		    align-items: flex-start;
		}
		.md-2 {
			-webkit-box-flex: 0 0 20%;      /* OLD - iOS 6-, Safari 3.1-6 */
			-moz-box-flex: 0 0 20%;         /* OLD - Firefox 19- */
			-webkit-flex: 0 0 20%;          /* Chrome */
			-ms-flex: 0 0 20%;              /* IE 10 */
			flex: 0 0 20%;   
			max-width: 20%;
		}
		.md-2 label {
			font-weight: bold;
		}
		.md-9 {
			-webkit-box-flex: 0 0 75%;      /* OLD - iOS 6-, Safari 3.1-6 */
			-moz-box-flex: 0 0 75%;         /* OLD - Firefox 19- */
			-webkit-flex: 0 0 75%;          /* Chrome */
			-ms-flex: 0 0 75%;              /* IE 10 */
			flex: 0 0 75%;  
			max-width: 75%; 
		}
		.md-9 .inputs {
			width: 100%;
		}
    </style>

	<div class="form_group staff-form-box">
		<div class="gap" style="margin-bottom: 10px;">
			<div class="md-2"><label style=";margin-right: 10px;">日程</label></div>
			<div class="md-9"><input type="text" name="schedule1" id="schedule1" class="inputs" value="<?php echo $schedule1; ?>"></div>
		</div>
		<div class="gap" style="margin-bottom: 10px;">
			<div class="md-2"><label style=";margin-right: 10px;">時間</label></div>
			<div class="md-9">
				<textarea name="schedule2" id="schedule2" class="inputs" rows="5" style="width: 100%;"><?php echo $schedule2; ?></textarea>
			</div>
		</div>
		<div class="gap" style="margin-bottom: 10px;">
			<div class="md-2"><label style=";margin-right: 10px;">会場</label></div>
			<div class="md-9"><input type="text" name="venue" id="venue" class="inputs" value="<?php echo $venue; ?>"></div>
		</div>
		<div class="gap" style="margin-bottom: 10px;">
			<div class="md-2"><label style="margin-right: 10px;">料金</label></div>
			<div class="md-9"><input type="text" name="price" id="price" class="inputs" value="<?php echo $price; ?>"></div>
		</div>

	</div>


    <?php
}

function event_meta_save( $post_id) {
	$is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );

    $is_valid_nonce_schedule1 = ( isset( $_POST[ 'schedule1' ] ) && wp_verify_nonce( $_POST[ 'schedule1' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    $is_valid_nonce_schedule2 = ( isset( $_POST[ 'schedule2' ] ) && wp_verify_nonce( $_POST[ 'schedule2' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    $is_valid_nonce_event = ( isset( $_POST[ 'venue' ] ) && wp_verify_nonce( $_POST[ 'venue' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    $is_valid_nonce_price = ( isset( $_POST[ 'price' ] ) && wp_verify_nonce( $_POST[ 'price' ], basename( __FILE__ ) ) ) ? 'true' : 'false';


    if ( $is_autosave || $is_revision || !$is_valid_nonce_schedule1 || !$is_valid_nonce_schedule2 || !$is_valid_nonce_event || !$is_valid_nonce_price  ) {
        return;
    }

    if ( isset( $_POST[ 'schedule1' ] ) || isset( $_POST[ 'schedule2' ] ) || isset( $_POST[ 'venue' ] ) || isset( $_POST[ 'price' ] ) ) {
    	update_post_meta( $post_id, 'schedule1', $_POST[ 'schedule1' ] );
    	update_post_meta( $post_id, 'schedule2', $_POST[ 'schedule2' ] );
    	update_post_meta( $post_id, 'venue', $_POST[ 'venue' ] );
    	update_post_meta( $post_id, 'price', $_POST[ 'price' ] );
    }
    
}
add_action( 'save_post', 'event_meta_save' );


// register gourmet stand custom post
function gourmet_stand_cpt_article() {
	$labels = [
		'name' => 'さくらグルメ屋台',
		'singular_name' => 'Gourmet Stand',
		'add_new_item' => 'Add New さくらグルメ屋台',
		'edit_item' => 'Edit さくらグルメ屋台'
	];

	$args = [
		'labels' 				=> $labels,
		'rewrite' => array( 'slug' => 'gourmet_stand'),
		'show_ui' 				=> true,
		'show_in_rest' 			=> true,
		'show_in_nav_menus'		=> true,
		'public'				=> false,
		'has_archive' 			=> false,
		'publicly_queryable' 	=> true,
		'query_var' 			=> true,
		'menu_icon'				=> 'dashicons-universal-access',
		'supports' 				=> ['title', 'editor', 'thumbnail', 'post-formats']
	];

	register_post_type('gourmet_stand', $args);
}

add_action('init', 'gourmet_stand_cpt_article');

// change enter title here in custom post type
function wpb_change_title_text( $title ){
     $screen = get_current_screen();
  
     if  ( 'gourmet_stand' == $screen->post_type ) {
          $title = 'Restaurant Name...';
     }
  
     return $title;
}
  
add_filter( 'enter_title_here', 'wpb_change_title_text' );

// area name custom field
function area_custom_metabox() {

	add_meta_box(
		'area_meta',
		__( 'Area Name' ),
		'area_metabox_callback',
		'gourmet_stand',
		'advance',
		'high'
	);
}


add_action( 'add_meta_boxes', 'area_custom_metabox' );

function area_metabox_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'area_name_once' );
	$wl_stored_meta = get_post_meta( $post->ID );

    $area_name = (!empty($wl_stored_meta['area_name']) ? esc_attr(  $wl_stored_meta['area_name'][0] ) : null);

    ?>

    <style>
    	.form-box {
    		max-width: 500px;
    		width: 100%;
    		position: relative;
    	}
    	.gap {
		  display: -webkit-box;
		  /* OLD - iOS 6-, Safari 3.1-6 */
		  /* OLD - Firefox 19- (buggy but mostly works) */
		  display: -ms-flexbox;
		  /* TWEENER - IE 10 */
		  display: -webkit-flex;
		  /* NEW - Chrome */
		  display: flex;
		  -webkit-flex-wrap: wrap;
		      -ms-flex-wrap: wrap;
		          flex-wrap: wrap;
		    align-items: flex-start;
		}

		.md-9 {
			-webkit-box-flex: 0 0 75%;      /* OLD - iOS 6-, Safari 3.1-6 */
			-moz-box-flex: 0 0 75%;         /* OLD - Firefox 19- */
			-webkit-flex: 0 0 75%;          /* Chrome */
			-ms-flex: 0 0 75%;              /* IE 10 */
			flex: 0 0 75%;  
			max-width: 75%; 
		}
		.md-9 .inputs {
			width: 100%;
		}
    </style>

    <div class="form_group form-box">
		<div class="gap" style="margin-bottom: 10px;">
			<div class="md-9"><input type="text" name="area_name" id="area_name" class="inputs" value="<?php echo $area_name; ?>"></div>
		</div>

	</div>

    <?php
}

function area_meta_save( $post_id) {
	$is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );

    $is_valid_nonce_area = ( isset( $_POST[ 'area_name' ] ) && wp_verify_nonce( $_POST[ 'area_name' ], basename( __FILE__ ) ) ) ? 'true' : 'false';


    if ( $is_autosave || $is_revision || !$is_valid_nonce_area ) {
        return;
    }

    if ( isset( $_POST[ 'area_name' ] ) ) {
    	update_post_meta( $post_id, 'area_name', $_POST[ 'area_name' ] );
    }
    
}
add_action( 'save_post', 'area_meta_save' );


/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function sakuram_matsuri_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'toho_kagu_pingback_header' );
