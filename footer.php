<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SAKURA MATSURI
 */

?>

    <!-- footer -->
    <?php  if(is_page( 'english' ) || is_page( 'korean' ) || is_page( 'chinese' )) {  ?>
        <footer class="bg-brown mt-100 mt-80-xs pt-50 pb-20 scroll_visible" id="footer">
            <div class="toTop inter">
                <div class="btn-toTop tr">
                    <a href="#" id="toTop">
                        <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/toTop.png" alt="" class="is-wide">
                    </a>
                </div>
            </div>
            <div class="cntr tc t-white">
                <h3 class="fs-24 fs-20-xs fw-500">
                   Organizer
                </h3>
                <h4 class="fs-20 fs-18-xs fw-500 mb-20 mb-15-xs">
                    Fukuoka Castle Sakura Festival Executive Committee
                </h4>
                <div class="tc">
                    <p class="t-white fs-12 fs-12-xs lh-38 lh-normal-xs">
                        COPYRIGHT ©︎ 福岡城さくらまつり実行委員会.<br class="v-xxs"> ALL RIGHT RESERVED. 
                    </p>
                </div>  
            </div>      
        </footer>
    <?php }else{ ?>

    <footer class="bg-brown pt-50 pb-20 scroll_visible" id="footer">
        <div class="toTop">
            <div class="btn-toTop tr">
                <a href="#" id="toTop">
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/toTop.png" alt="to_top" class="is-wide">
                </a>
            </div>
        </div>
        <div class="cntr">
            <div class="gap gap-4-md gap-2-sm gap-0-xs row-rev-xs">
                <div class="md-9 sm-8 xs-12">
                    <div class="footer-infos t-white">
                        <h2 class="fs-24 fs-24-xs fw-500 lh-38 mb-20">主催</h2>
                        <h3 class="fs-20 fs-20-xs fw-500 lh-38 mb-20">福岡城さくらまつり実行委員会</h3>
                        <p class="fs-14 fs-14-xs lh-22">
                            福岡市／公益財団法人 福岡市緑のまちづくり協会／福岡商工会議所／<br class="v-pc">
                            公益財団法人 福岡観光コンベンションビューロー／NPO 法人 福岡城市民の会／赤坂校区自治協議会
                        </p>
                        <!-- <h3 class="fs-20 fw-500 lh-38 mb-30">照明協力…COLOR KINETICS JAPAN</h3> -->

                        <h2 class="fs-24 fs-24-xs fw-500 lh-38 mt-30 mt-30-sp">お問い合わせ</h2>
                        <h3 class="fs-14 fs-14-xs fw-500 lh-38">
                            福岡城さくらまつり実行委員会事務局（福岡市住宅都市局みどり推進課）<br>
                            TEL：<a href="tel:0927114424" class="tdecor-none t-white">092-711-4424</a>（受付時間/10:00〜17:30 ※平日のみ）
                        </h3>
                    </div>
                </div>
                <div class="md-3 sm-4 xs-12">
                    <div class="footer-logo">
                        <h1 class="logo">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/logo.png" alt="logo" class="is-wide">
                            </a>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <p class="t-white fs-12 fs-12-xs lh-38 lh-normal-xs">
                    COPYRIGHT ©︎ 福岡城さくらまつり実行委員会.<br class="v-xxs"> ALL RIGHT RESERVED. 
                </p>
            </div>  
        </div>      
    </footer>

    <?php } ?>
</body>

    <?php wp_footer(); ?>


    <?php if(is_home() || is_front_page()) {  ?>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
        <script>
            
            if($(window).innerWidth()<751) {

                var $owl = $('.owl-carousel');

                $owl.children().each( function( index ) {
                  $(this).attr( 'data-position', index ); // NB: .attr() instead of .data()
                });

                $owl.owlCarousel({
                    nav: false,
                    center: true,
                    dots: false,
                    autoplay: true,
                    touchDrag: true,
                    mouseDrag: true,
                    items: 4,
                    loop: true,
                    margin: 20,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: false
                });

                
                $(window).load(function() {

                    var data_target = $('.owl-item.active.center').find('a').attr('data-target');
                    $('.gallery-main').attr('data-passed', data_target);
                    if(data_target==null || data_target=='') {
                        $('#play_btn').css({
                            opacity: 0
                        });
                    }else{
                        $('#play_btn').css({
                            opacity: 1
                        });
                    }

                    var img_src = $('.owl-item.active.center').find('.gallery-item').attr('parent-img');
                    $('.gallery-main').css({
                        opacity: 1,
                        transition: 'all ease-in-out .5s',
                        background: 'url('+tem_dir+'/assets/img/cards/slider/'+img_src+') center center / cover no-repeat'
                    });
                });

                // jQuery method on
                $owl.on('changed.owl.carousel',function(property){
                    var current = property.item.index;
                    var src = $(property.target).find(".owl-item").eq(current).find(".gallery-item").attr('parent-img');

                    var data_target = $(property.target).find(".owl-item").eq(current).find(".gallery-item").children('a').attr('data-target');

                    if(data_target==null || data_target=='') {
                        $('#play_btn').css({
                            opacity: 0
                        });
                        $('.gallery-main').attr('data-passed', '');
                    }else{
                        $('#play_btn').css({
                            opacity: 1
                        });
                        $('.gallery-main').attr('data-passed', data_target);
                    }

                    

                    $('.gallery-main').css({
                        opacity: 1,
                        transition: 'all ease-in-out .5s',
                        background: 'url('+tem_dir+'/assets/img/cards/slider/'+src+') center center / cover no-repeat'
                    });

                });
                $(document).on('click', '.owl-item>div', function(e) {
                    e.preventDefault();

                    var img_src = $(this).attr('parent-img');
                    var data_target = $(this).find('a').attr('data-target');

                    $('.gallery-main').attr('data-passed', data_target);

                    var data_passed = $('.gallery-main').attr('data-passed');

                    $('.gallery-item > a').removeClass('is-active');
                    $(this).find('a').addClass('is-active');

                    
                   

                     if(data_passed==null || data_passed=='') {
                        $('#play_btn').css({
                            opacity: 0
                        });
                    }else{
                        $('#play_btn').css({
                            opacity: 1
                        });
                    }
                    

                    $owl.trigger('to.owl.carousel', $(this).data( 'position' ) );


                    $('.gallery-main').css({
                        opacity: 1,
                        transition: 'all ease-in-out .5s',
                        background: 'url('+tem_dir+'/assets/img/cards/slider/'+img_src+') center center / cover no-repeat'
                    });
                    
                });
            }

        </script>

    <?php } ?>

     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.2.2/masonry.pkgd.min.js"></script>
       

    <?php $is_home = ''; if(is_home() || is_front_page()) { $is_home = 'is-home';  ?>


        <script>
                

            ( function($) {


                $(document).on('click','.cat__btn-list li > a',function(e){
                    e.preventDefault();

                    var num_attrib = $(this).attr('num-attr');

                    if($(this).hasClass('is-active')) {
                        $('.cat__btn-list li > a').removeClass('is-active');
                        $('.information-lists').find('.sakura_item').css({
                            visibility:'visible',
                            display: 'block'
                        });
                        $('#info_moreBtn').attr('href','information');
                            $('.sakura_masonry').masonry();
                    
                    }else{
                        $('.cat__btn-list li > a').removeClass('is-active');

                        $(this).addClass('is-active');

                        $('.information-lists').find('.sakura_item').css({
                            visibility: 'hidden',
                            display: 'none'
                        });

                        $('.information-lists').find('.sakura_item[data-attr='+num_attrib+']').css({
                            visibility:'visible',
                            display: 'block'
                        });

                        $('#info_moreBtn').attr('href','information?cat='+num_attrib);
                        
                            $('.sakura_masonry').masonry();
    
                    }
                    
                    
                });

            }) (jQuery)

        </script>

        
    <?php }else{ ?>

        <script>
            
            ( function($) {

                function info_loop() {
                    $('#p_info li').each(function(){
                        
                        var cat_attr = $(this).find('a').attr('cat-attr');
                        var own_attr = $(this).find('a').attr('own-attr');
                        $(this).find('a').attr('href', '?cat='+own_attr);
                        if(cat_attr==own_attr) {
                            $(this).find('a').addClass('is-active');
                        }


                    });
                }

                info_loop();

                $(document).on('mouseover','.cat__btn-list li > a',function(e){

                    var cat_attr = $(this).attr('cat-attr');
                    var own_attr = $(this).attr('own-attr');

                    if(cat_attr!=='test') {
                        if(cat_attr==own_attr) {
                            $(this).attr('href', window.location.href.split("?")[0]);
                        }
                    }

                    
                });

                $(document).on('mouseout','.cat__btn-list li > a',function(e){
                    info_loop();
                });


            }) (jQuery)

        </script>

    <?php } ?>

    <script>
            
            $(window).load(function(){

            if($(window).innerWidth()>767) { 
                  var $container = $('.sakura_masonry');
                  // initialize
                  $container.masonry({
                    columnWidth: 50,
                    horizontalOrder: true,
                    itemSelector: '.sakura_item',
                    percentPosition: true,
                    isAnimated: true,
                      animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                      }
                  })
            }
            });

    </script>

</html>
