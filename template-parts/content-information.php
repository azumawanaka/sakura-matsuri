<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SAKURA_MATSURI
 */

?>



	<section class="wrp mb-90 mb-50-xs">
		<div class="banner banner-information"></div>

		<div class="banner-bottom bg-bluedot">
			<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/flower-petals3.png" alt="" class="illustrations is-tr is-tr2 wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">
			<div class="cntr">
				<h2 class="title for-lower info-title wow fadeInUp" data-wow-duration="1.8s" style="visibility: visible; animation-duration: 1.8s; animation-name: fadeInUp;">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/info-txt.png" alt="" class="t_img">
				</h2>
				<div class="breacrumbs">
					<ul>
						<li>
							<span>
								イベント・開花情報
							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="wrp bg-white mb-80 mb-80-xs">
		
		<div class="cntr">
			<div class="gap gap-5-md gap-1-xs mb-100 mb-50-xs jc-c">
				<div class="md-1 xs-4 wow fadeInUp pr-0 mb-30-xs" data-wow-duration="1s">
					<?php 
						$categories = get_the_terms( $post->ID, 'categories' );
	                    // get term id
	                    $cat_tID = $categories[0]->term_id;
	                    $img_name = "flower-green.png";

	                    $article_date = get_the_date('Y.m.d');

	                    if($cat_tID==3) {
	                    	$img_name = "flower-green.png";
	                    } else if ($cat_tID==4) {

	                    	$img_name = "flower-pink.png";
	                    } else {
	                    	$img_name = "flower-blue.png";
	                    } 
	                ?>
					<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/<?php echo $img_name; ?>" alt="" class="is-wide">
				</div>
				<div class="md-6 xs-12 wow fadeInUp" data-wow-duration="1s">
					<div class="article-infos">
						<?php if($cat_tID!==3) { ?>
							<p class="fs-14 t-blue fw-800"><?php echo get_the_date('Y.m.d'); ?></p>
						<?php }else{
							
							if(get_post_meta($post->ID,'input1', true)==1) {
								echo '<span class="btn is-border pickup">開催中</span>';
							}
							
						} ?>
						
						<h4 class="fs-24 t-dark mb-60 mb-40-xs"><?php the_title(); ?></h4>
						<div class="fs-16 mb-50 mb-20-xs">
							<?php $content = get_the_content(); 
							$content = apply_filters( 'the_content', $content );
							echo $content; ?>
						</div>
						<?php if($cat_tID==3) { ?>
							<?php if(get_post_meta($post->ID,'schedule1', true)!=='') { ?>
								<h5 class="title has-iconleft t-orange fs-18 fs-16-xs mb-10 mb-10-xs"><span>日程</span></h5>
							<?php } ?>
							<p class="fs-16 mb-30 mb-20-xs">
								<?php echo nl2br (get_post_meta($post->ID,'schedule1', true) ); ?>
							</p>	
							<?php if(get_post_meta($post->ID,'schedule2', true)!=='') { ?>
								<h5 class="title has-iconleft t-orange fs-18 fs-16-xs mb-10 mb-10-xs"><span>時間</span></h5>
							<?php } ?>
							<p class="fs-16 mb-30 mb-20-xs">
								<?php echo nl2br (get_post_meta($post->ID,'schedule2', true) ); ?>
							</p>	
							<?php if(get_post_meta($post->ID,'venue', true)!=='') { ?>
								<h5 class="title has-iconleft t-orange fs-18 fs-16-xs mb-10 mb-10-xs"><span>会場</span></h5>
							<?php }?>
							<p class="fs-16 mb-30 mb-20-xs">
								<?php echo get_post_meta($post->ID,'venue', true); ?>
							</p>	
							<?php if(get_post_meta($post->ID,'price', true)!=='') { ?>
								<h5 class="title has-iconleft t-orange fs-18 fs-16-xs mb-10 mb-10-xs"><span>料金</span></h5>
							<?php }?>
							<p class="fs-16 mb-30 mb-20-xs">
								<?php echo get_post_meta($post->ID,'price', true); ?>
							</p>	
						<?php } ?>
					</div>
				</div>
				<div class="md-5 xs-12 wow fadeInUp" data-wow-duration="1.5s">
					<?php if(has_post_thumbnail()){ ?>
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="is-wide" />
					<?php }else{
						if($cat_tID==2) {
						?>
						<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-04.png" alt="<?php the_title(); ?>"  class="is-wide"  />
					<?php } else if($cat_tID==3) { ?>
						<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-green.png" alt="<?php the_title(); ?>"  class="is-wide" />
						<?php } else if($cat_tID==4) { ?>
						<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-pink.png" alt="<?php the_title(); ?>"  class="is-wide" />
						<?php }
					} ?>
				</div>
			</div>		

			<div class="info-more tc wow fadeInUp" data-wow-duration="1s">
				<a href="information" class="btn bg-blue has-icon icon-arrow">一覧へ戻る</a>
			</div>	

		</div>
		<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/illus-3.png" alt="" class="illustrations is-br2 v-pc">
	</section>