<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Sakura_Matsuri 
 * @since Sakura Matsuri 1.0
 */


get_header();
?>
    <section class="hero scroll_visible" id="hero">
            <div class="hero-content tc">
                <div class="hero-img">
                    <!-- <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/hero-txt.png" alt="" class="h_i s1"> -->
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/logo.png" alt="" class="h_i s2">
                </div>
                <div class="cntr">
                    <div class="hero-text-bt">
                        <h1>
                            <span>平成31年</span>
                            3月下旬〜4月上旬
                        </h1>
                        <p class="fw-500">
                            今年の開催日程は、さくらの開花予想発表後、<br class="v-pc">
                            3月中旬に発表します。
                        </p>
                        <a href="<?php echo esc_url( home_url( '/gourmet' ) ); ?>" class="t-pink2 fs-16 fs-16-xs">「さくらBBQ」予約はこちら</a>
                    </div>  
                </div>
            </div>
            <div class="cntr tc fadeScrollAnimate">
                <div class="fadeScroll">
                    <span>SCROLL</span>
                </div>
            </div>
    </section>
    
    <section class="wrp mb-160 mb-90-xs">
        <div class="pos-rel bg-white">
            <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/illus-3.png" alt="" class="illustrations is-tr wow fadeInUp v-pc" data-wow-duration="2s" data-wow-offset="100">
        </div>
    </section>
    <section class="wrp bg-pink pad-l-sp pad-l-xs  mb-80 mb-50-xs" id="sched">
        <div class="cntr scroll_visible"  id="information">
            <h2 class="title is-abs pos-tl is-index wow fadeInUp" data-wow-duration="1.8s">
                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/schedule-txt.png" alt="" class="t_img">
            </h2>
        </div>
        <div class="cntr-wide pb-80 pb-40-xs pt-70 pt-35-xs">
            <div class="w50-per mb-30-sp">
                <div class="cntr-half is-left pad-r-sp pad-r-xs">
                    <div class="wow fadeInUp mb-50 mb-40-xs" data-wow-duration="1s">
                        <h4 class="fs-18 t-brown">開催期間</h4>
                        <h2 class="fw-500 fs-28 lh-40">平成31年3月下旬〜4月上旬</h2>
                        <p class="fw-500">
                            今年の開催日程は、さくらの開花予想発表後、<br class="v-pc">3月中旬に発表します。
                        </p>

                        <h4 class="fs-18 t-brown mt-30 mt-20-sp">場所</h4>
                        <h2 class="fw-500 fs-28 lh-40 mb-30 mb-30-xs">舞鶴公園（国指定史跡・福岡城跡）</h2>

                        <div class="cntr-info">
                            <p class="fw-500">
                                インフォメーション開設時間…10：00～22：00<br>
                                ライトアップ点灯時間…18：00～22：00
                            </p>
                        </div>  
                    </div>
                    <div class="btn-cntr mb-20 wow fadeInUp" data-wow-duration="1.2s">
                        <div class="div-btn jc-c-xs">
                            <a href="<?php echo esc_url( home_url( '/access' ) ); ?>" class="btn bg-blue has-icon icon-arrow mlr-auto-xs">アクセス</a>
                            <a href="#" class="btn bg-pink100 has-icon icon-sqs mlr-auto-xs">場内マップ</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w50-per pos-rel index-1 pl-50 pl-0-xs">
                <a href="<?php echo esc_url( home_url( '/sakura-info' ) ); ?>"  class="card bg-card_1 t-white p-35 p-20-xs wow fadeInUp" data-wow-duration="1s">
                    <div class="cntr-half is-right pad-r-30 pad-r-30-sp-no">
                        <div class="card-head">
                            <div class="card-title">
                                <h2 class="fs-20 fs-18-xs fw-500 mb-20 mb-15-xs lh-40">さくらと城壁のライトアップについて</h2>
                            </div>
                        </div>
                        <div class="card-body mb-35 mb-20-xs">
                            <div class="card-infos">
                                <p class="fs-14 fs-14-xs t-white">
                                    春になると舞鶴公園には約1,000本の桜が咲き誇ります。<br class="v-pc">昼と夜とで表情を変えるさくらをお楽しみください。
                                </p>
                            </div>
                        </div>
                        <div class="btn-cntr">
                            <span class="btn p-0">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/arrow-lg.png" alt="" width="56">
                            </span>
                        </div>
                    </div>
                </a>
                <a href="<?php echo esc_url( home_url( '/gourmet' ) ); ?>" class="card bg-card_2 t-white p-35 p-20-xs wow fadeInUp" data-wow-duration="1.5s">
                    <div class="cntr-half is-right pad-r-30 pad-r-30-sp-no">
                        <div class="card-head">
                            <div class="card-title fs-20 fs-18-xs fw-500 mb-20 mb-15-xs">
                                グルメについて
                            </div>
                        </div>
                        <div class="card-body mb-35 mb-20-xs">
                            <div class="card-infos">
                                <p class="fs-14 fs-14-xs  t-white">
                                    総数90軒を超えるバラエティ豊かなグルメ屋台と<br class="v-pc">キッチンカーが大集合。
                                </p>
                            </div>
                        </div>
                        <div class="btn-cntr">
                            <span class="btn p-0">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/arrow-lg.png" alt="" width="56">
                            </span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>

    <section class="wrp bg-white">
        <div class="cntr wow fadeInUp" data-wow-duration="1s">

            <div class="gap gap-0-md gap-0-xs ai-c mb-70 mb-35-xs">
                <div class="md-6 xs-12">
                    <h2 class="title pos-tl wow fadeInUp mb-15-xs" data-wow-duration="1.8s">
                        <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/info-txt.png" alt="" class="big_title">
                    </h2>
                </div>
            </div>

            <div class="information-pickup">
                <div class="gap gap-5-md gap-0-xs mb-40 mb-30-xs sakura_masonry">

                        <?php 
                            $paged2 = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                            $wpb_all_query2 = new WP_Query(
                                        array(
                                            'post_type'     =>'article', 
                                            'post_status'   =>'publish', 
                                            'posts_per_page' => -1, 
                                            'orderby'        => 'publish_date',
                                            'order'         => 'DESC',
                                            'paged'         => $paged2
                                        ));  

                            ?>

                          <?php if ( $wpb_all_query2->have_posts() ) : ?>
                          <!-- the loop -->
                            

                          <?php while ( $wpb_all_query2->have_posts() ) : $wpb_all_query2->the_post();  ?>
                            <?php 
                                    $categories2 = get_the_terms( $post->ID, 'categories' );
                                    // get term id
                                    $cat_tID2 = $categories2[0]->term_id;
                                    $img_name2 = "flower-green.png";

                                    $article_date2 = get_the_date('Y.m.d');
              
                                    if($cat_tID2==3 && get_post_meta($post->ID,'input1', true)==1) {
                                        $img_name2 = "flower-green.png";
                                    
                                ?>
                                <div class="md-4 xs-12 mb-90 mb-50-xs sakura_item"  data-attr="<?php echo $cat_tID; ?>">    
                                
                                    <a href="<?php the_permalink(); ?>" class="card">
                                        <div class="card-body">
                                            <div class="card-img">
                                                <div class="flwr-icon">
                                                    
                                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/<?php echo $img_name2; ?>" alt="">
                                                    
                                                </div>

                                                <?php if(has_post_thumbnail()){ ?>
                                                    <img class="is-wide" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                                                <?php }else{ ?>

                                                    <img  class="is-wide" src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-green.png" alt="<?php the_title(); ?>"">
                                                <?php } ?>

                                            </div>

                                               <?php if(get_post_meta($post->ID,'schedule1', true)) {
                                                    echo '<div class="mt-5">'.get_post_meta($post->ID,'schedule1', true).'</div>';
                                                }
                                                if(get_post_meta($post->ID,'input1', true)==1) {
                                                    echo '<span class="btn is-border pickup">開催中</span>';
                                                }  ?>

                                            <h4 class="info-title"><?php the_title(); ?></h4>
                                            <div class="info-txt">
                                                <?php $content2 = get_the_content(); 
                                                    // strip tags to avoid breaking any html
                                                    $string2 = strip_tags($content2);
                                                    if (strlen($string2) > 110) {

                                                        // truncate string
                                                        $stringCut2 = mb_strcut($string2, 0, 110);
                                                        $endPoint2 = strrpos($stringCut2, ' ');

                                                        //if the string doesn't contain any space then it will cut without word basis.
                                                        $string2 = $endPoint2? mb_strcut($stringCut2, 0, $endPoint2) : mb_strcut($stringCut2, 0);
                                                        $string2 .= '...';
                                                    }
                                                    echo $string2;

                                                 ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>     

                                <?php } ?>     
                          <?php endwhile; ?>

                      <!-- end of the loop -->

                    <?php endif; ?>
                </div>


            </div>

            <div class="gap gap-0-md gap-0-xs ai-c mb-100 mb-90-xs jc-c">
                <div class="md-6 xs-12 mb--35 mb-0-xs">
                    <ul class="cat__btn-list mb-50-xs jc-c" id="p_info">
                        <?php
                           $args = array(
                                       'taxonomy' => 'categories',
                                       'orderby' => 'term_id',
                                       'order'   => 'ASC'
                                   );

                           $cats = get_categories($args);
                           $img_name = "is-green";
                           foreach($cats as $cat) {

                            if($cat->term_id==3) {
                                $img_name = "is-green";
                            } else if ($cat->term_id==4) {

                                $img_name = "is-pink";
                            } else {
                                $img_name = "is-blue";
                            } 
                        ?>
                            <li>
                                <a href="#" class="btn is-border <?php echo $img_name; ?>" num-attr="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></a>
                            </li>
                        <?php
                           }
                        ?>
                        </ul>
                </div>
            </div>

            <div class="information-lists">
                <div class="gap gap-5-md gap-0-xs mb-40 mb-30-xs sakura_masonry">

                        <?php 
                            $count = 0;
                            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                            $wpb_all_query = new WP_Query(
                                        array(
                                            'post_type'     =>'article', 
                                            'post_status'   =>'publish', 
                                            'posts_per_page' => 9, 
                                            'orderby'        => 'publish_date',
                                            'order'         => 'DESC',
                                            'paged'         => $paged
                                        ));  

                            $total_pages = $wpb_all_query->max_num_pages;       
                            ?>

                          <?php if ( $wpb_all_query->have_posts() ) : ?>
                          <!-- the loop -->
                            

                          <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); $count++; ?>
                            <?php 
                                    $categories = get_the_terms( $post->ID, 'categories' );
                                    // get term id
                                    $cat_tID = $categories[0]->term_id;
                                    $img_name = "flower-green.png";

                                    $article_date = get_the_date('Y.m.d');
              
                                    if($cat_tID==3) {
                                        $img_name = "flower-green.png";
                                    } else if ($cat_tID==4) {

                                        $img_name = "flower-pink.png";
                                    } else {
                                        $img_name = "flower-blue.png";
                                    } 
                                ?>
                                <div class="md-4 xs-12 mb-90 mb-50-xs sakura_item"  data-attr="<?php echo $cat_tID; ?>">    
                                
                                    <a href="<?php the_permalink(); ?>" class="card">
                                        <div class="card-body">
                                            <div class="card-img">
                                                <div class="flwr-icon">
                                                    
                                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/<?php echo $img_name; ?>" alt="">
                                                    
                                                </div>

                                                <?php if(has_post_thumbnail()){ ?>
                                                    <img class="is-wide" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                                                <?php }else{
                                                    if($cat_tID==2) {
                                                    ?>
                                                    <img  class="is-wide" src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-04.png" alt="<?php the_title(); ?>">
                                                <?php } else if($cat_tID==3) { ?>
                                                    <img  class="is-wide" src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-green.png" alt="<?php the_title(); ?>"">
                                                    <?php } else if($cat_tID==4) { ?>
                                                    <img  class="is-wide" src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-pink.png" alt="<?php the_title(); ?>" >
                                                    <?php }
                                                } ?>

                                            </div>
                                            <?php if($cat_tID!==3) { ?>
                                                <!-- <p class="info-date"><?php echo $article_date; ?></p> -->
                                            <?php }else{
                                               echo '<div class="mt-5">'.get_post_meta($post->ID,'schedule1', true).'</div>';
                                            
                                                if(get_post_meta($post->ID,'input1', true)==1) {
                                                    echo '<span class="btn is-border pickup">開催中</span>';
                                                }
                                                
                                            } ?>
                                            <h4 class="info-title"><?php the_title(); ?></h4>
                                            <div class="info-txt">
                                                <?php $content = get_the_content(); 
                                                    // strip tags to avoid breaking any html
                                                    $string = strip_tags($content);
                                                    if (strlen($string) > 110) {

                                                        // truncate string
                                                        $stringCut = mb_strcut($string, 0, 110);
                                                        $endPoint = strrpos($stringCut, ' ');

                                                        //if the string doesn't contain any space then it will cut without word basis.
                                                        $string = $endPoint? mb_strcut($stringCut, 0, $endPoint) : mb_strcut($stringCut, 0);
                                                        $string .= '...';
                                                    }
                                                    echo $string;

                                                 ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>          
                          <?php endwhile; ?>

                      <!-- end of the loop -->

                    <?php endif; ?>
                </div>

                <div class="info-more tc">
                    <a href="information" class="btn bg-blue has-icon icon-arrow" id="info_moreBtn">もっと見る</a>
                </div>

            </div>
            
        </div>
    </section>

    <section class="wrp h-400 h-200-sp bg-white flow-hidden-xs mt-40-xs" >
        <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/petals.png" alt="" class="illustrations is-bl wow fadeInUp" data-wow-duration="1s">
        <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/tower.png" alt="" class="illustrations is-br wow fadeInUp" data-wow-duration="1s">
    </section>


    <section class="wrp bg-bluedot gallery-wrp">
        <div class="gallery-sec">
            <div class="gallery-body">
                
                <div class="cw-70-per">

                    <div class="gallery-top-txt">
                        <div class="scroll_visible">
                            <h2 class="title is-abs pos-tl wow fadeInUp" data-wow-duration="1.8s">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gal-txt.png" alt="" class="t_img">
                            </h2>
                        </div>
                        <h4 class="fs-18 fs-16-xs fw-500 t-white">
                            見て、遊んで、食べて、飲んで…。<br>
                            ひとりひとり違ったさくらまつりの楽しみ方があります。
                        </h4>
                    </div>
                    <div class="gallery-main tc">
                        <a href="#" id="play_btn">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/play_btn.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="cw-30-per">
                    <div class="gallery-slider">
                        <div class="gallery-pc owl-carousel" id="gallery_slider">
                            <div class="item gallery-item" parent-img="slide-1-lg.png">
                                <a href="#" data-target="1">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/slider/slide-1.png" alt="" class="is-wide">
                                </a>
                            </div>
                            <div class="item gallery-item" parent-img="slide-2-lg.png">
                                <a href="#" class="is-active" data-target="2">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/slider/slide-2.png" alt="" class="is-wide">
                                </a>
                            </div>
                            <div class="item gallery-item" parent-img="slide-3-lg.png">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/slider/slide-3.png" alt="" class="is-wide">
                                </a>
                            </div>
                            <div class="item gallery-item" parent-img="slide-4-lg.png">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/slider/slide-4.png" alt="" class="is-wide">
                                </a>
                            </div>
                            <div class="item gallery-item" parent-img="slide-5-lg.png">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/slider/slide-5.png" alt="" class="is-wide">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

    <section class="wrp bg-white flow-hidden-xs mt-120 mt-50-xs h-180 h-auto-xs mb-80 mb-50-xs">
        <div class="gap gap-0-md gap-0-xs jc-c tc">
            <div class="md-4 xs-10">
                <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/illus-4.png" alt="" class="is-wide">
            </div>
        </div>
    </section>

    <section class="wrp">
        <div class="cntr">
            <h3 class="title lh-40 lh-30-xs is-border t-orange fs-24 fs-18-xs">特別協賛</h3>
            <div class="logos">
                <ul class="logo-list is-typeA is-one-sp  jc-c-xs">
                    <li>
                        <a href="https://www.nishinippon.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_nishinihonshinbun.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.asahibeer.co.jp/area/10/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_asahi.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.mizoe.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_mizoe.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.pieroth.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_pieroth.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.boatrace-fukuoka.com/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_boatrace.png" alt="">
                        </a>
                    </li>
                </ul>
            </div>
            <h3 class="title lh-40 lh-30-xs is-border t-orange fs-24 fs-18-xs">協賛</h3>
            <div class="logos">
                <ul class="logo-list is-typeA">
                    <li>
                        <a href="https://www.fukuokabank.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_fukugin.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.miyoshi.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_miyoshi.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.jrhakatacity.com/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_amu.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.sekisuihouse.co.jp/gm/fuk/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_sekisuihouse.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://fukuoka-toyopet.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_toyopet.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.dream-stage.net/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_dreamstage.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.nishitetsu.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_nishitetsu.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.jti.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_jt.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.mizutaki-yamato.com/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_yamato.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.softbankhawks.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_softbank.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://f-jumokui.com/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_jumokuikai.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ncbank.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_nbc.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://corporate.shinnihonseiyaku.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_shinnihon.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.fukuya.com/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_fukuya.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://fukuokashigyokyo.com/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_gyogyou.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.heiwadai-hotel.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_heiwadai.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://f-sizokyo.or.jp/kyoukai.php" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_zouen.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://fukuoka-ga.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_ryokakyoukai.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://www.pietro.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_pietoro.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="http://subway.city.fukuoka.lg.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_chikatetsu.gif" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/sp_encrest.jpg" alt="">
                        </a>
                    </li>
                </ul>
            </div>
            <h3 class="title lh-40 lh-30-xs is-border t-orange fs-24 fs-18-xs">照明協力</h3>
            <div class="logos">
                <ul class="logo-list jc-c-xs">
                    <li>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/logos/color_kinetics.png" alt="" class="is-wide">
                        </a>
                    </li>
                </ul>
            </div>
            <h3 class="title lh-40 lh-30-xs is-border t-orange fs-24 fs-18-xs">周辺施設のご案内</h3>
            <div class="logos">
                <ul class="logo-list tc">
                    <li>
                        <div class="card mb-15-xs">
                            <a href="https://www.midorimachi.jp/maiduru/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-01.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">舞鶴公園</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="http://fukuokajyo.com/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-02.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">福岡城むかし探訪館</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="https://www.ohorikouen.jp/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-03.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">大濠公園/西公園</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="https://www.ohoriteien.jp/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-04.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">大濠公園日本庭園</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="http://www.ohori-nougaku.jp/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-05.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">大濠公園能楽堂</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="http://www.oohoriboathouse.jp/" target="_blank">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-06.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">ボートハウス 大濠パーク</span>
                        </div>
                    </li>
                    <li>
                        <div class="card mb-15-xs">
                            <a href="https://www.fukuoka-art-museum.jp/">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/cards/card-xs/card-07.png" alt="" class="is-wide">
                            </a>
                            <span class="fw-500">福岡市美術館</span>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </section>

    <!-- videos modal -->
    <div class="vid-modal">
        <div class="vid-cntr">
            <a href="#" id="close-vid">
                <span></span>
            </a>
            <div class="vid-body">
                <iframe src="https://www.youtube.com/embed/C3uorfS_TIY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%; height: 346px;"></iframe>
            </div>
        </div>
    </div>
    <!-- //end videos modal -->
<?php
get_footer();
