<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SAKURA_MATSURI
 */

get_header();
?>

	<section class="wrp mb-90 mb-50-xs">
		<div class="banner banner-gourmet"></div>

		<div class="banner-bottom bg-bluedot">
			<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/flower-petals3.png" alt="" class="illustrations is-tr is-tr2 wow fadeInUp v-pc" data-wow-duration="2s">

			<div class="cntr">
				<h2 class="title gourmet-title for-lower wow fadeInUp" data-wow-duration="1.8s" style="visibility: visible; animation-duration: 1.8s; animation-name: fadeInUp;">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet-txt.png" alt="" class="t_img">
				</h2>
				<div class="breacrumbs">
					<ul>
						<li>
							<span>グルメ・BBQ</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
	</section>
	
	<section class="wrp mb-130 mb-90-xs ovflow-x-hidden">

		<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/cloud-form.png" alt="" class="illustrations is-tr is-tr2 wow fadeInUp v-pc" data-wow-duration="2.5s">

		<div class="pos-rel bg-white">		

			<div class="cntr">
		
				<div class="gap gap-7-md gap-0-xs ai-c">
					<div class="md-9 xs-12 wow fadeInUp" data-wow-duration="1s">
						<h3 class="title fs-28 fs-28-xs t-orange fw-500 mb-15 mb-15-xs">さくらBBQ</h3>
						<p class="mb-70 mb-100-xs fw-500">
							福岡城さくらまつり開催期間中に限り、舞鶴公園の西広場でBBQの実施が可能になります。<br>
							下記の会場ルールを守って、バーベキューを楽しみましょう！
						</p>
					</div>
				</div>
					
				<div class="gap gap-5-md gap-0-xs mb-30-xs mb-50">
					<div class="md-9 xs-12">
						<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-15 mb-15-xs"><span>BBQの利用可能日について</span></h5>
						<p class="mb-28 mb-20-xs fw-500">
							BBQをご利用される場合、下記の日程で実施が可能です。<br>
							<span class="fs-24 fs-20-xs text-bold">3月27日（水）～3月31日（日）</span><br>
							※福岡城さくらまつりの開催日は3月中旬に決定しますので、日程が決まり次第、追加の予約日程を更新します。
						</p>
					</div>
					<div class="md-9 xs-12">
						<h6 class="title t-blue fs-16 fs-16-xs mb-15 mb-15-xs">ご利用可能な場所</h6>
						<p class="mb-28 mb-20-xs fw-500">西広場（下記MAPをご覧ください）</p>
					</div>
					<div class="md-10 xs-12">
						<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/map.jpg" alt="" class="is-wide">
					</div>
				</div>
					
				<div class="gap gap-7-md gap-0-xs ai-c">
					<div class="md-9 xs-12 wow fadeInUp" data-wow-duration="1s">

						<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-15 mb-15-xs"><span>BBQの利用方法</span></h5>
						<p class="mb-50 mb-50-xs fw-500 text-bold text-use">
							①個人または団体で食材／道具などを持ち込む方法<br>
							②さくらまつりが指定する業者に依頼する方法（手ぶらでBBQ）<br>
							いずれかをお選びください。<br>
							※常設施設「Green Magic MAIZURU」でもBBQを利用することが可能です。詳しくは<a href="#green-magic-maizuru" class="t-pink2 go-to">こちら</a>
						</p>



						<h6 class="title t-blue fs-16 fs-16-xs">さくらBBQ広場 指定業者のご案内</h6>
						<p class="mb-28 mb-20-xs fw-500 pl-20">
							②を利用する場合、指定業者は、以下5社に直接ご予約ください。<br>
							・<a href="http://bbq-group.jp/" class="t-pink2" target="_blank">BBQ王ホットフィールズ</a>：092-741-7700<br class="v-sp">（対応時間 10：00～19：00）<br>
							・<a href="http://bbqmasters.jp/" class="t-pink2" target="_blank">BBQ MASTERS</a>：092-321-0411<br class="v-sp">（対応時間 11：00～21：00）<br>
							・<a href="https://www.manpuku.co.jp/bbq_fukuoka/products?category_id=345" class="t-pink2" target="_blank">出張ＢＢＱ満福福岡</a>：080-2799-2071<br class="v-sp">（対応時間 9：00～18：00）<br>
							・<a href="http://bbq.works-e.net/" class="t-pink2" target="_blank">手ぶらBBQワークス</a>：0120-377-029<br class="v-sp">（対応時間 平日10:00～18:00）<br>
							・<a href="https://bb-qtarou.com/" class="t-pink2" target="_blank">BBQ太郎</a>：092-410-3029<br class="v-sp">（対応時間 10:00～21:00）<br>
							<span class="text-bold">※電話が込み合う時期ですのでご理解の程、よろしくお願い致します。</span><br>
							<span class="text-attention">※指定業者以外でのBBQサービス実施は出来ません。予めご了承ください。</span>
						</p>

						<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-15 mb-15-xs"><span>BBQの利用時間について</span></h5>
						<p class="mb-28 mb-20-xs fw-500">
							①、②いずれの方法でも下記の時間のみ利用が可能です。<br>
							<span class="fs-20 fs-18-xs">BBQの利用時間：10：00～22：00</span><br>

						</p>

						<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-15 mb-15-xs"><span>BBQの利用に係る運営協力金について</span></h5>
						<p class="mb-28 mb-20-xs fw-500">
							運営協力金は、公園内の火気の適正利用や巡回指導・美化活動のための費用に充てさせて頂きます。<br>
							<span class="text-attention">［運営協力金］お一人様200円（小学生以上）</span><br>
							①を利用される場合：西広場のBBQ受付でお支払い下さい（当日受付　※受付場所はMAP参照）。<br>
							②を利用される場合：利用されるBBQ指定業者の受付に直接お支払いください。<br>
						</p>
						<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-15 mb-15-xs"><span>駐車場について</span></h5>
						<p class="fw-500 pl-20 mb-15 mb-15-xs">
							<span class="text-attention">西広場には一般利用の駐車場はございませんので、</span>付近の有料駐車場をご利用ください（場所はMAP参照）。
						</p>
						<dl class="text-bold mb-28 mb-20-xs"><span class="text-bold">車でお越しのお客様（駐車場のご案内）</span>
							<dt>①タイムズ福岡城三の丸(施設最寄り駐車場)<br> ◎営業時間/7:00~21:00(※出庫は24:00まで)</dt>
							<dt>②舞鶴公園第1駐車場<br> ◎営業時間/5:30~20:00(4月~10月)6:30~19:00(11月~3月)(※出庫は24:00まで)<br> ◎休業日/12/29~1/3</dt>
							<dt>③大濠公園有料駐車場<br> ◎営業時間/7:00~23:00(※出庫は23:00まで)</dt>
						</dl>
					
					</div>
					<div class="md-3 xs-12 wow fadeInUp v-pc" data-wow-duration="2s">
						<div class="r-img mt-0 mt-30-xs">
							<ul class="img-list">
								<li>
									<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet-01.png" alt="" class="is-wide">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet-02.png" alt="" class="is-wide">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet-03.png" alt="" class="is-wide">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet-04.png" alt="" class="is-wide">
								</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="gap gap-5-md gap-0-xs mb-30-xs">
					<div class="md-9 xs-12">
						<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-15 mb-15-xs" ><span>その他公園利用の注意事項</span></h5>
						<p class="mb-28 mb-20-xs fw-500">
							・できる限りごみの持ち帰りにご協力ください <br>
							・文化財，樹木付近や園路，運動施設では火気を使わないでください。<br>
							・花見当日以外の場所取り及び視線を遮る幕等の使用を禁止します。<br>
							・騒音やボール遊び等の迷惑行為を禁止します。<br>
							・無許可での営業行為を禁止します。 <br>
							・スタッフや警備員の指示・注意に従っていただけない場合は、ご利用をお断りすることがございます。<br>
							※福岡市の公園は火気の使用を原則禁止していますが、さくらまつり期間中は条件がございますが利用は可能です。詳しい条件についてはこちらの<a href="<?php echo get_template_directory_uri()?>/assets/pdf/HandbookonFireUse.pdf" class="t-pink2" target="_blank">火気利用の手引き</a>を御覧ください。
						</p>
					</div>
				</div>
				<h5 class="title has-iconleft t-orange fs-18 fs-18-xs mb-15 mb-15-xs" id="green-magic-maizuru"><span>常設施設「Green Magic MAIZURU」について</span></h5>
				<p class="mb-15 mb-15-xs fw-500">
					舞鶴公園・西広場には、カフェ、ランニングステーション、図書館を併設した、本格アメリカン<br class="v-pc">バーベキューが楽しめる常設施設「Green Magic MAIZURU」がございます。
				</p>

				<div class="gap gap-0-md gap-0-xs">
					<div class="md-5 xs-12">
						<p class="fw-500">「Green Magic MAIZURU」ご利用の方はこちら</p>
						<a href="http://green-magic-maizuru.jp" target="_blank"><img src="<?php echo get_template_directory_uri()?>/assets/img/cards/greenmagic-card.png" alt="" class="is-wide"></a>
					</div>
				</div>

				<div class="r-img mt-0 mt-30-xs v-sp">
					<ul class="img-list">
						<li>
							<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet-01.png" alt="" class="is-wide">
						</li>
						<li>
							<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet-02.png" alt="" class="is-wide">
						</li>
						<li>
							<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet-03.png" alt="" class="is-wide">
						</li>
						<li>
							<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet-04.png" alt="" class="is-wide">
						</li>
					</ul>
				</div>

			</div>

		</div>
	</section>

	<section class="wrp bg-pink2 pt-50 pt-30-xs pb-100 pb-80-xs">
		<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/illus-3.png" alt="" class="illustrations  is-tr  is-tr3 wow fadeInUp" data-wow-duration="1.2s">
		<div class="cntr wow fadeInUp" data-wow-duration="1.5s">
			<h3 class="title fs-28 fs-20-xs t-orange fw-500 mb-20 mb-15-xs">さくらグルメ屋台</h3>
			<p>
				総数90軒を超えるバラエティ豊かなグルメ屋台とキッチンカーが大集合。<br class="v-pc">花見や散歩や夜
				の宴会など、さまざまなシーンでお楽しみいただける充実したメニューを提供します。<br>
				
			</p>

			<div class="gap gap-5-md gap-0-xs mt-60 mt-40-xs">

				<?php 

		          		$wpb_all_query = new WP_Query(
	          				array(
	          					'post_type'		=>'gourmet_stand', 
	          					'post_status'	=>'publish', 
	          					'posts_per_page'=> -1,
	          					'order' 		=> 'DESC',
	          					'orderby'		=> 'publish_date'
	          				)); 

          				 ?>

	        	<?php if ( $wpb_all_query->have_posts() ) : ?>
			          
			        <!-- the loop -->
			        <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); $count++; ?>
			        
			        <div class="md-4 xs-12 mb-40 mb-30-xs">
			        	<div class="card">
							<div class="card-body">
								<div class="card-img bg-white tc mb-20 mb-15-xs ovflow-hidden pad-rand">
									<?php if(has_post_thumbnail()){ ?>
										<img src="<?php echo the_post_thumbnail_url("medium") ?>" alt="<?php the_title(); ?>" class="h-180 h-160-xs">
									<?php }else{ ?>
										<img src="<?php echo get_template_directory_uri()?>/assets/img/cards/gourmet.png" alt=""  class="is-wide">
									<?php } ?>
									
								</div>
								<div class="info-title">
									<small class="fs-14 fs-14-xs fw-800 t-blue"><?php echo get_post_meta($post->ID,'area_name', true); ?></small>
									<h5 class="title fs-18 fs-16-xs t-dark fw-500"><?php echo get_the_title(); ?></h5>
								</div>
								<div class="info-txt fs-14 fs-14-xs">
									<?php echo nl2br(get_the_content()); ?>
								</div>
							</div>
						</div>

			        </div>
			        <?php endwhile; ?>
			        <!-- end of the loop -->
			    <?php endif; ?>
			</div>

		</div>
	</section>

<?php
get_footer();
?>
<span style="text-decoration:underline;"></span>