<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Sakura_Matsuri 
 * @since Sakura Matsuri 1.0
 */


get_header();
?>

    <section class="top-head">
        <div class="head">
            <header class="header-main">
                <h3 class="logo <?php echo $is_home; ?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri()?>/assets/img/icons/logo.png" alt="logo"></a></h3>
            </header>
            <a href="javascript:;" id="toggle-nav">
                <span></span>
                <span></span>
                <span></span>
            </a>
            <nav class="nav-main is-sp">
                <ul class="nav-item-l">
                    <li>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" <?php if(is_home()) { echo "class='is-active'"; } ?>>トップ</a>
                    </li>
                    <li class="dropDown">
                        <span class="drop-icon toggle-sub"></span>
                        <a href="<?php echo esc_url( home_url( '/information' ) ); ?>" class="parent-link">イベント・開花情報</a>
                        <ul class="sub-items">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/information/?cat=2' ) ); ?>">新着ニュース</a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url( home_url( '/information/?cat=3' ) ); ?>">イベント</a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url( home_url( '/information/?cat=4' ) ); ?>">さくら開花</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo esc_url( home_url( '/sakura-info' ) ); ?>"  class="<?php if(is_page('sakura-info')) { echo "is-active"; } ?>">さくらライトアップ情報</a>
                    </li>
                    <li>
                        <a href="<?php echo esc_url( home_url( '/gourmet' ) ); ?>"  class="<?php if(is_page('gourmet')) { echo "is-active"; } ?>">グルメ・BBQ</a>
                    </li>
                    <li>
                        <a href="<?php echo esc_url( home_url( '/access' ) ); ?>"   class="<?php if(is_page('access')) { echo "is-active"; } ?>">アクセス</a>
                    </li>
                </ul>
                <ul class="nav-item-r">
                    <li class="dropDown">
                        <span class="drop-icon toggle-sub"></span>

                        <?php 

                                $arr_lang = array(
                                    array("JP",''),
                                    array("EN", 'english'),
                                    array('中文', 'chinese'),
                                    array('한국', 'korean')
                                );

                                echo '<a href="javascript:;" class="parent-link">JP</a>';
                                echo '<ul class="sub-items">';
                                foreach ($arr_lang as $page_lang) {
                                    
                                    if( $page_lang[1] !== is_page() ) {

                                ?>
                                    
                                    <li>
                                        <a href="<?php echo esc_url( home_url( '/'.$page_lang[1] ) ); ?>"><?php echo $page_lang[0]; ?></a>
                                    </li>

                                <?php }
                                }
                                echo '</ul>';
                         ?>
                       

                    </li>
                    <li class="nav-icons">
                        <a href="https://twitter.com/infosakura2012" target="_blank" class="is-tw">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/twitter.png" alt="">
                        </a>
                        <a href="https://www.facebook.com/infosakura2012/?ref=br_rs" target="_blank" class="is-fb">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/fb.png" alt="">
                        </a>
                        <a href="https://www.instagram.com/fukuoka_sakuramatsuri/" target="_blank" class="is-fb">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/instagram.png" alt="">
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>

<?php
get_footer();
